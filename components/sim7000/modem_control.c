#include "modem_control.h"

esp_err_t _init_gpios()
{
    uint64_t pintBitMask = ((1ULL << PWRKEY) | (1ULL << NRESET) | (1ULL << MODEM_POWER));
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = pintBitMask;
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    esp_err_t sts = gpio_config(&io_conf);
    if (sts != ESP_OK)
        ESP_LOGI("MAIN", "reset pin not set");
    //INPUTS
    pintBitMask = ((1ULL << SIM7000_RI) | (1ULL << SIM7000_STATUS));
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = pintBitMask;

    sts = gpio_config(&io_conf);
    gpio_set_intr_type(SIM7000_RI, GPIO_INTR_NEGEDGE);
    gpio_install_isr_service(0);
    if (sts != ESP_OK)
        ESP_LOGI("MAIN", "reset pin not set");
    return sts;

    return sts;
}

boolean is_modem_on()
{
    return gpio_get_level(SIM7000_STATUS);
}

boolean IR_active()
{
    return IR_status;
}

void turn_off_modem()
{
    if (is_modem_on())
    {

        ESP_LOGW("turn_off_modem", "Apagando modem, desinstalando driver!");
#ifdef UART_ESP
        if (uart_is_driver_installed(UART_NUM))
            uart_driver_delete(UART_NUM);
#else
        if (serial_modem != NULL)
            sw_del(serial_modem);
#endif
        gpio_set_level(PWRKEY, 0);
        vTaskDelay(500 / portTICK_PERIOD_MS);
        gpio_set_level(PWRKEY, 1);
        vTaskDelay(1500 / portTICK_PERIOD_MS);
        gpio_set_level(PWRKEY, 0);
        uint8_t cont = 40;
        while (cont-- > 0 && is_modem_on())
        {
            vTaskDelay(500 / portTICK_PERIOD_MS);
        }
        ESP_LOGI("TURN OFF MODEM", "STATUS: %d Half sec: %d", is_modem_on(), cont);
        //vTaskDelay(7000 / portTICK_PERIOD_MS);
        gpio_set_level(MODEM_POWER, 0);
        vTaskDelay(1500 / portTICK_PERIOD_MS);
    }
}

esp_err_t turn_on_modem()
{
    if (!is_modem_on())
    {
        gpio_set_level(MODEM_POWER, 0);
        vTaskDelay(500 / portTICK_PERIOD_MS);
        gpio_set_level(MODEM_POWER, 1);
        vTaskDelay(500 / portTICK_PERIOD_MS);
        gpio_set_level(NRESET, 0);
        gpio_set_level(PWRKEY, 0);
        vTaskDelay(500 / portTICK_PERIOD_MS);
        gpio_set_level(PWRKEY, 1);
        vTaskDelay(1100 / portTICK_PERIOD_MS);
        gpio_set_level(PWRKEY, 0);
        uint8_t cont = 0;
        while (cont++ < 100 && !is_modem_on())
        {
            vTaskDelay(500 / portTICK_PERIOD_MS);
        }
        ESP_LOGW("TURN ON MODEM", "STATUS: %d Half sec: %d", is_modem_on(), cont);
        if (is_modem_on())
        {
            esp_err_t sts;
            sts = start_uart();
            if (sts != ESP_OK)
            {
                ESP_LOGE("turn_on_modem", "Failed START UART");
            }
            else
                sts = initialize_sim7000_uart();
            ESP_LOGW("turn_on_modem", "modem,  driver %d!", sts);
            return sts;
        }
    }
    return ESP_FAIL;
}


esp_err_t  reset_modem()
{
    if (!is_modem_on())
        return turn_on_modem();
    gpio_set_level(NRESET, 1);
    vTaskDelay(500 / portTICK_PERIOD_MS);
    gpio_set_level(NRESET, 0);
    vTaskDelay(500 / portTICK_PERIOD_MS);
    int cont = 0;
    while (cont++ < 1000 && !is_modem_on())
    {
        vTaskDelay(10 / portTICK_PERIOD_MS);
    }
    ESP_LOGW("fast_reset_modem", "STATUS: %d Half sec: %d", is_modem_on(), cont);
    //vTaskDelay(1000 / portTICK_PERIOD_MS);
    return !is_modem_on();

}


void task_delay_start(void *arg)
{
    _init_gpios();
    turn_on_modem();
    if(start_uart() == ESP_OK){
        esp_err_t resp = start_up_sim(0);
        while(resp != ESP_OK){
            reset_modem();
            vTaskDelay(2000 / portTICK_PERIOD_MS);
            resp = start_up_sim(0);
        }
        is_sim_connected(0);
    }  
    vTaskDelete(NULL);
}

void delay_start_up(){
    xTaskCreate(task_delay_start, "task_delay_start", 2048, NULL, 10, NULL);
}
esp_err_t start_up_sim(uint8_t force_conn)
{
    int start = esp_timer_get_time() / 1000;
    esp_err_t err = initialize_sim7000_uart();
    if (err == ESP_OK)
    {
        if (force_conn)
        {
            send_check_reply("AT+CGREG=2,2", OK_REPLY, 3000, true);
        }
        else
        {
            set_functionality(1, 1);
        }
        set_operating_band("NB-IOT", 1);
        set_operating_band("NB-IOT", 3);
        set_operating_band("NB-IOT", 5);
        set_operating_band("CAT-M", 1);
        set_operating_band("CAT-M", 3);
        set_operating_band("CAT-M", 5);
        indicate_RI_using_URC(1);
        set_preferred_mode(2);
        set_preferred_LTE_mode(3);
        set_network_settings(APN, USER, PWD);
        //APAGAR GPS MODEM
        GPS_modem_state = 1;
        start = esp_timer_get_time() / 1000;
        if (!enable_GPS(false) && esp_timer_get_time() / 1000 - start < ENABLE_GPS_TIMEOUT)
        {
            ESP_LOGE("start_up_sim", "Failed to turn off GPS, retrying...");
            vTaskDelay(2000 / portTICK_PERIOD_MS); // Retry every 2s
        }
    }
    else
    {
        ESP_LOGE("start_up_sim", "Failed initialize_sim7000_uart %d...", err);
    }
    return err;
}

/*
* @brief is_sim_connected: verify MODEM connection
* @param fast_process : only check if exist SIM
* @return    1 : OK
            -1 : TIME OUT 
            -2 : TIME OUT trying connect to network
            -3 : TIME OUT trying open wireless connection
*/

int8_t is_sim_connected(boolean fast_process)
{
    
    ESP_LOGI("is_sim_connected", "is_sim_connected Fast ? %d\n", fast_process);
    if (fast_process == 1)
    {
        return is_there_a_sim(2000) ? 1 : -1;
    }
    if (!is_there_a_sim(11000))
    {
        return -1;
    }
    //connect_manually_operator();
    int start = esp_timer_get_time() / 1000;
    get_RSSI();
    int cont = 0;
    uint8_t n = net_status();
    while (!(n == 1 || n == 5))
    {
        n = net_status();
        //TODO corregir lo del contador: debe intentar ambas cosas sin retornar .... hasta cumplir timeout... 
        if (n == 0 && cont++ > 160)
        {
            set_network_registration(2);
            n = net_status();
            if (n == 0)
                return -7;
        }
        if ((esp_timer_get_time() / 1000 - start) > CONNECTING_CELL_NETWORK_TIMEOUT)
        {
            
            send_check_reply("AT+CGREG=2,2", OK_REPLY, 3000, true);
            n = net_status();
            if ((n == 1 || n == 5))
                break;

            return -2;
        }
       
        ESP_LOGI("SIM", "Failed to connect to cell network, retrying... RSSID %d ", get_RSSI());
        vTaskDelay(2000 / portTICK_PERIOD_MS); // Retry every 2s
    }
    ESP_LOGI("SIM", "Connected to cell network!");
    start = esp_timer_get_time() / 1000;
    while (!wireless_conn_status())
    {
        while (!open_wireless_connection(true))
        {
            if ((esp_timer_get_time() / 1000 - start) > OPENING_WIRELESS_CONNECTION_TIMEOUT)
            {
                return -3;
            }
            ESP_LOGI("SIM", "Failed to enable connection, retrying...");
            vTaskDelay(2000 / portTICK_PERIOD_MS); // Retry every 2s
        }
        net_status();
        get_RSSI();
        enable_GPRS(true);
        
        if (wireless_conn_status())
            ESP_LOGI("SIM", "Enabled data!");
    }
    
    ESP_LOGI("SIM", "Data already enabled!");
    
    return 1;
}



