
#include "sim7000.h"

boolean show_info = true;
boolean sincronize_at(int16_t timeout)
{
    int start = esp_timer_get_time() / 1000;
    int length = 0;
    while ((esp_timer_get_time() / 1000 - start) < timeout)
    {
//uart_flush(UART_NUM);
#ifdef UART_ESP
        uart_write_bytes(UART_NUM, "AT\r", 3);
#else
        sw_write_bytes(serial_modem, (uint8_t *)"AT\r", 3);
#endif
        length = available_uart();
        while (length <= 0 && (esp_timer_get_time() / 1000 - start) < timeout / 10)
        {
            length = available_uart();
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }
        if (show_info)
            ESP_LOGI("SIM7000", "CONFIRMING AT > %d ms", (int)(esp_timer_get_time() / 1000 - start));
        if ((esp_timer_get_time() / 1000 - start) >= timeout)
        {
            return false;
        }
#ifdef UART_ESP
        uart_read_bytes(UART_NUM, (uint8_t *)replybuffer, length, timeout / portTICK_RATE_MS);
#else
        sw_read_bytes(serial_modem, (uint8_t *)replybuffer, length, timeout);
#endif
        //readline(DEFAULT_TIMEOUT, false);
        //ESP_LOGI("SIM7000", "CONFIRMING AT > (%d) %s", length, replybuffer);
        if (strstr(replybuffer, "AT") != NULL || strstr(replybuffer, "OK") != NULL)
            return true;

        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
    if (show_info)
        ESP_LOGI("SIM7000", "sincronize_at timeout %d", (int)(esp_timer_get_time() / 1000 - start));
    return false;
}
#ifdef UART_ESP
const uart_config_t uart_config2 = {
    .baud_rate = 115200,
    .data_bits = UART_DATA_8_BITS,
    .parity = UART_PARITY_DISABLE,
    .stop_bits = UART_STOP_BITS_1,
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE};
#endif

boolean is_there_a_sim(int timeout)
{
    int start = esp_timer_get_time() / 1000;
    while ((esp_timer_get_time() / 1000) - start < timeout)
    {
        get_reply("AT+CPIN?", 5000, true);
        if (strstr(replybuffer, "+CPIN: READY") != NULL)
        {
            if (show_info)
                ESP_LOGI("SIM7000", "SIM READY!!!");
            return true;
        }
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
    if (show_info)
        ESP_LOGI("SIM7000", "SIM NOT READY :(!");
    return false;
}

esp_err_t start_uart()
{
#ifdef UART_ESP
    if (!uart_is_driver_installed(UART_NUM))
    {
        esp_log_level_set(TAG_SIM, ESP_LOG_INFO);
        uart_param_config(UART_NUM, &uart_config2);
        uart_set_pin(UART_NUM, TXD_MODEM, RXD_MODEM, RTS_MODEM, CTS_MODEM);
        return uart_driver_install(UART_NUM, RX_BUF_SIZE, 0, 0, NULL, 0);
    }
    else
    {
        return ESP_OK;
    }

    //return uart_set_sw_flow_ctrl(UART_NUM, true, 17, 19);
#else
    serial_modem = sw_new(TXD_MODEM, RXD_MODEM, false, RX_BUF_SIZE);
    if (serial_modem != NULL)
        return sw_open(serial_modem, 9600);
    return -1;
#endif
}

esp_err_t initialize_sim7000_uart()
{
    firstTime = true;
    esp_err_t resp = ESP_OK;
    // give 7 seconds to reboot
    if (sincronize_at(15000) && is_there_a_sim(11000))
    {
        
        if (enable_PSM_TAU(false, "00101100", "00000110"))
        {
            enable_sleep_mode(false);
        }
        
        set_local_data_flow_control(0, 0);
        send_check_reply("ATE0", OK_REPLY, DEFAULT_TIMEOUT, true);
        vTaskDelay(100 / portTICK_PERIOD_MS);
        
        get_reply("AT+GSN", 2000, true);
        strncpy(IMEI, replybuffer + 2, 15);
        ESP_LOGI(TAG_SIM, "IMEI %s", IMEI);
        get_reply("AT+CGMR", 1000, true);
        vTaskDelay(100 / portTICK_PERIOD_MS);
        get_reply("ATI", 1000, true);

        if (strstr(replybuffer, ("SIM808 R14")) != 0)
        {
            _type = SIM808_V2;
        }
        else if (strstr(replybuffer, ("SIM808 R13")) != 0)
        {
            _type = SIM808_V1;
        }
        else if (strstr(replybuffer, ("SIM800 R13")) != 0)
        {
            _type = SIM800L;
        }
        else if (strstr(replybuffer, ("SIMCOM_SIM5320A")) != 0)
        {
            _type = SIM5320A;
        }
        else if (strstr(replybuffer, ("SIMCOM_SIM5320E")) != 0)
        {
            _type = SIM5320E;
        }
        else if (strstr(replybuffer, ("SIM7000A")) != 0)
        {
            _type = SIM7000A;
        }
        else if (strstr(replybuffer, ("SIM7000C")) != 0)
        {
            _type = SIM7000C;
        }
        else if (strstr(replybuffer, ("SIM7000E")) != 0)
        {
            _type = SIM7000E;
        }
        else if (strstr(replybuffer, ("SIM7000G")) != 0)
        {
            _type = SIM7000G;
        }
        else if (strstr(replybuffer, ("SIM7500A")) != 0)
        {
            _type = SIM7500A;
        }
        else if (strstr(replybuffer, ("SIM7500E")) != 0)
        {
            _type = SIM7500E;
        }
        else if (strstr(replybuffer, ("SIM7600A")) != 0)
        {
            _type = SIM7600A;
        }
        else if (strstr(replybuffer, ("SIM7600C")) != 0)
        {
            _type = SIM7600C;
        }
        else if (strstr(replybuffer, ("SIM7600E")) != 0)
        {
            _type = SIM7600E;
        }
        if (show_info)
            ESP_LOGI("SIM7000", "TYPE %d", _type);
        if (_type == 0)
            resp = -1;
    }
    else
    {
        resp = -2;
    }

    return resp;
}
// SET TE-TA Local Data Flow Control
// Parameters
// 0 No flow control
// 1 Software flow control
// 2 Hardware flow control
boolean set_local_data_flow_control(uint8_t dce_by_dte, uint8_t dte_by_dce)
{
    char cmdBuff[24];
    sprintf(cmdBuff, "AT+IFC=%d,%d", dce_by_dte, dte_by_dce);
    return send_check_reply(cmdBuff, OK_REPLY, DEFAULT_TIMEOUT, false);
}

//  Report Mobile Equipment Erro
//Parameters <n>
// 0    Disable +CME ERROR: <err> result code and use ERROR instead.
// 1    Enable +CME ERROR: <err> result code and use numeri c <err>
// 2 Enable +CME ERROR: <err> result code and use verbose <err>
boolean set_report_mobile_equipment_error(uint8_t error_lvl)
{
    char cmdBuff[24];
    sprintf(cmdBuff, "AT+CMEE=%d", error_lvl);
    return send_check_reply(cmdBuff, OK_REPLY, DEFAULT_TIMEOUT, false);
}

// Uses the AT+CFUN command to set functionality (refer to AT+CFUN in manual)
// 0 --> Minimum functionality
// 1 --> Full functionality
// 4 --> Disable RF
// 5 --> Factory test mode
// 6 --> Reset module
// 7 --> Offline mode
boolean set_functionality(uint8_t option, uint8_t reset_MT)
{
    char cmdBuff[24];
    sprintf(cmdBuff, "AT+CFUN=%d", option /*, reset_MT*/);
    return send_check_reply(cmdBuff, OK_REPLY, 2500, false);
}

// 2  - Automatic
// 13 - GSM only
// 38 - LTE only
// 51 - GSM and LTE only
boolean set_preferred_mode(uint8_t mode)
{
    char cmdBuff[24];
    sprintf(cmdBuff, "AT+CNMP=%d", mode);
    return send_check_reply(cmdBuff, OK_REPLY, 2000, false);
}

// 1 - CAT-M
// 2 - NB-IoT
// 3 - CAT-M and NB-IoT
boolean set_preferred_LTE_mode(uint8_t mode)
{
    char cmdBuff[24];
    sprintf(cmdBuff, "AT+CMNB=%d", mode);
    return send_check_reply(cmdBuff, OK_REPLY, DEFAULT_TIMEOUT, false);
}

// Useful for choosing a certain carrier only
// For example, AT&T uses band 12 in the US for LTE CAT-M
// whereas Verizon uses band 13
// Mode: "CAT-M" or "NB-IOT"
// Band: The cellular EUTRAN band number
boolean set_operating_band(const char *mode, uint8_t band)
{
    char cmdBuff[24];
    sprintf(cmdBuff, "AT+CBANDCFG=\"%s\",%i", mode, band);
    return send_check_reply(cmdBuff, OK_REPLY, DEFAULT_TIMEOUT, false);
}

void set_network_settings(char *papn, char *pusername, char *ppassword)
{
    //char cmdBuff[100];
    apn = papn;
    apnusername = pusername;
    apnpassword = ppassword;
    useragent = "ESP32";
    if (_type >= SIM7000A)
    {
        send_check_reply_quoted("AT+CGDCONT=1,\"IP\",", apn, OK_REPLY, 10000, false);
    }
}

void set_HTTPS_redirect(boolean onoff)
{
    httpsredirect = onoff;
}

// Set Indicate RI when using URC
// Mode options:
// 0  off
// 1  On(TCPIP, FTP and URC control RI pin)
// 2  On(only TCPIP control RI pin)
boolean indicate_RI_using_URC(uint8_t mode)
{
    char cmdBuff[24];
    sprintf(cmdBuff, "AT+CFGRI=%d", mode);
    return send_check_reply(cmdBuff, OK_REPLY, DEFAULT_TIMEOUT, true);
}

boolean enable_RTC(boolean i)
{
    char cmdBuff[24];
    sprintf(cmdBuff, "AT+CLTS=%d", i);
    if (!send_check_reply(cmdBuff, OK_REPLY, DEFAULT_TIMEOUT, false))
        return false;

    return send_check_reply("AT&W", OK_REPLY, DEFAULT_TIMEOUT, false);
}

// Sleep mode reduces power consumption significantly while remaining registered to the network
// NOTE: USB port must be disconnected before this will take effect
boolean enable_sleep_mode(boolean onoff)
{
    char cmdBuff[24];
    sprintf(cmdBuff, "AT+CSCLK=%d", onoff);
    return send_check_reply(cmdBuff, OK_REPLY, DEFAULT_TIMEOUT, false);
}

// Set e-RX parameters
// Mode options:
// 0  Disable the use of eDRX
// 1  Enable the use of eDRX
// 2  Enable the use of eDRX and auto report
// 3  Disable the use of eDRX(Reserved)

// Connection type options:
// 4 - CAT-M
// 5 - NB-IoT

// See AT command manual for eDRX values (options 0-15)

// NOTE: Network must support eDRX mode
boolean set_eDRX(uint8_t mode, uint8_t connType, char *eDRX_val)
{
    if (strlen(eDRX_val) > 4)
        return false;
    char auxStr[21];
    sprintf(auxStr, "AT+CEDRXS=%d,%d,%s", mode, connType, eDRX_val);
    return send_check_reply(auxStr, OK_REPLY, DEFAULT_TIMEOUT, false);
}

// Set PSM with custom TAU and active time
// For both TAU and Active time, leftmost 3 bits represent the multiplier and rightmost 5 bits represent the value in bits.

// For TAU, left 3 bits:
// 000 10min
// 001 1hr
// 010 10hr
// 011 2s
// 100 30s
// 101 1min
// For Active time, left 3 bits:
// 000 2s
// 001 1min
// 010 6min
// 111 disabled
// Calculator: https://www.thalesgroup.com/en/markets/digital-identity-and-security/iot/resources/developers/psm-calculation-tool
// Note: Network decides the final value of the TAU and active time.
boolean enable_PSM_TAU(boolean onoff, char *TAU_val, char *activeTime_val)
{ // AT+CPSMS command
    if (strlen(activeTime_val) > 8)
        return false;
    if (strlen(TAU_val) > 8)
        return false;
    ESP_LOGW(TAG_SIM, "enable_PSM: %d", onoff);
    enable_sleep_mode(onoff);
    char auxStr[35];
    sprintf(auxStr, "AT+CPSMS=%i,,,\"%s\",\"%s\"", onoff, TAU_val, activeTime_val);

    return send_check_reply(auxStr, OK_REPLY, DEFAULT_TIMEOUT, false);
}

// NOTE: Network must support PSM and modem needs to restart before it takes effect
boolean enable_PSM(boolean onoff)
{
    ESP_LOGW(TAG_SIM, "enable_PSM: %d", onoff);
    enable_sleep_mode(onoff);
    char auxStr[21];
    sprintf(auxStr, "AT+CPSMS=%d", onoff);
    return send_check_reply(auxStr, OK_REPLY, DEFAULT_TIMEOUT, false);
}

boolean read_PSM_dynamic_parameters()
{
    send_check_reply("AT+CPSMRDP", OK_REPLY, 2 * DEFAULT_TIMEOUT, false);
    send_check_reply("AT+CPSMRDP?", OK_REPLY, 2 * DEFAULT_TIMEOUT, false);
    send_check_reply("AT+CPSMCFG=?", OK_REPLY, 2 * DEFAULT_TIMEOUT, false);
    send_check_reply("AT+CPSMCFG?", OK_REPLY, 2 * DEFAULT_TIMEOUT, false);
    send_check_reply("AT+CPSMCFGEXT=?", OK_REPLY, 2 * DEFAULT_TIMEOUT, false);
    send_check_reply("AT+CPSMCFGEXT?", OK_REPLY, 2 * DEFAULT_TIMEOUT, false);
    send_check_reply("AT+CPSMSTATUS=?", OK_REPLY, 2 * DEFAULT_TIMEOUT, false);
    send_check_reply("AT+CPSMSTATUS?", OK_REPLY, 2 * DEFAULT_TIMEOUT, false);
    return true;
}

//GNSS Work Mode Set
//<GPS mode> GPS work mode
//      1 Start GPS NMEA out
// <glo mode> GLONASS work mode
//      0 Stop GLONASS NMEA out
//      1 Start GLONASS NMEA out
// <bd mode> BEIDOU work mode
//      0 Stop BEIDOU NMEA out
//      1 Start BEIDOU NMEA out
//      2 BEIDOU outside of us
// <ga mode> GALILEAN work mode
//      0 Stop GALILEAN NMEA out
//      1 Start GALILEAN NMEA out
//      2 GALILEAN out side of us
boolean GPS_work_mode_set(uint8_t gps_mode, uint8_t glo_mode, uint8_t bd_mode, uint8_t gal_mode)
{
    char buff[50];
    sprintf(buff, "AT+CGNSMOD=%d,%d,%d,%d", gps_mode, glo_mode, bd_mode, gal_mode);
    return send_check_reply(buff, OK_REPLY, DEFAULT_TIMEOUT, true);
}

boolean enable_GPS(boolean onoff)
{
    if (GPS_modem_state == onoff)
        return true;
    uint16_t state = 0;

    // First check if its already on or off
    if (_type == SIM808_V2 || _type == SIM7000A || _type == SIM7000C || _type == SIM7000E || _type == SIM7000G)
    {
        if (!send_parse_reply("AT+CGNSPWR?", "+CGNSPWR: ", &state, ',', 0, 10000, true))
            return false;
    }
    else if (_type == SIM5320A || _type == SIM5320E || _type == SIM7500A || _type == SIM7500E || _type == SIM7600A || _type == SIM7600C || _type == SIM7600E)
    {
        if (!send_parse_reply("AT+CGPS?", "+CGPS: ", &state, ',', 0, 10000, true))
            return false;
    }
    else
    {
        if (!send_parse_reply("AT+CGPSPWR?", "+CGPSPWR: ", &state, ',', 0, 10000, true))
            return false;
    }
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    if (onoff && !state)
    {
        if (_type == SIM808_V2 || _type == SIM7000A || _type == SIM7000C || _type == SIM7000E || _type == SIM7000G)
        {
            if (!send_check_reply("AT+CGNSPWR=1", OK_REPLY, 10000, true))
                return false;
        }
        else if (_type == SIM5320A || _type == SIM5320E || _type == SIM7500A || _type == SIM7500E || _type == SIM7600A || _type == SIM7600C || _type == SIM7600E)
        {
            if (!send_check_reply("AT+CGPS=1", OK_REPLY, 10000, true))
                return false;
        }
        else
        {
            if (!send_check_reply("AT+CGPSPWR=1", OK_REPLY, 10000, true))
                return false;
        }
    }
    else if (!onoff && state)
    {
        if (_type == SIM808_V2 || _type == SIM7000A || _type == SIM7000C || _type == SIM7000E || _type == SIM7000G)
        {
            if (!send_check_reply("AT+CGNSPWR=0", OK_REPLY, DEFAULT_TIMEOUT, true))
                return false;
        }
        else if (_type == SIM5320A || _type == SIM5320E || _type == SIM7500A || _type == SIM7500E || _type == SIM7600A || _type == SIM7600C || _type == SIM7600E)
        {
            if (!send_check_reply("AT+CGPS=0", OK_REPLY, DEFAULT_TIMEOUT, true))
                return false;
            // this takes a little time
            readline(2000, false); // eat '+CGPS: 0'
        }
        else
        {
            if (!send_check_reply("AT+CGPSPWR=0", OK_REPLY, DEFAULT_TIMEOUT, true))
                return false;
        }
    }
    GPS_modem_state = onoff;
    return true;
}

// Query wireless connection status
boolean wireless_conn_status(void)
{
    get_reply("AT+CNACT?", DEFAULT_TIMEOUT * 5, true);
    // Format of response:
    // +CNACT: <status>,<ip_addr>
    if (strstr(replybuffer, "+CNACT: 1") != NULL || strstr(replybuffer, "+CNACT: 2") != NULL)
        return true;
    return false;
}

// Open or close wireless data connection
boolean open_wireless_connection(boolean onoff)
{
    if (!onoff)
        return send_check_reply("AT+CNACT=0", OK_REPLY, DEFAULT_TIMEOUT, true); // Disconnect wireless
    else
    {
        /**/
        send_check_reply_quoted("AT+CNACT=1,", apn, OK_REPLY, 20000, true);
        if (strstr(replybuffer, "+APP PDP: ACTIVE") != NULL)
            return true;
        else{
            return wireless_conn_status();
        }
            
    }
}

boolean send_parse_reply(char *tosend, char *toreply, uint16_t *v, char divider, uint8_t index, uint32_t timeout, boolean multiline)
{

    get_reply(tosend, timeout, multiline);
    if (!parse_reply(toreply, v, divider, index))
        return false;
    if (!multiline)
        readline(DEFAULT_TIMEOUT, false); // eat 'OK'

    return true;
}

boolean parse_reply(char *toreply, uint16_t *v, char divider, uint8_t index)
{
    char *p = strstr(replybuffer, toreply); // get the pointer to the voltage
    if (p == NULL)
        return false;
    p += strlen(toreply);

    for (uint8_t i = 0; i < index; i++)
    {
        // increment dividers
        p = strchr(p, divider);
        if (!p)
            return false;
        p++;
    }
    *v = atoi(p);

    return true;
}

boolean parse_reply_quoted(char *toreply, char *v, int maxlen, char divider, uint8_t index)
{
    uint8_t i = 0, j;
    // Verify response starts with toreply.
    char *p = strstr(replybuffer, toreply);
    if (p == 0)
        return false;
    p += strlen(toreply);
    // Find location of desired response field.
    for (i = 0; i < index; i++)
    {
        // increment dividers
        p = strchr(p, divider);
        if (!p)
            return false;
        p++;
    }

    // Copy characters from response field into result string.
    for (i = 0, j = 0; j < maxlen && i < strlen(p); ++i)
    {
        // Stop if a divier is found.
        if (p[i] == divider)
            break;
        // Skip any quotation marks.
        else if (p[i] == '"')
            continue;
        v[j++] = p[i];
    }

    // Add a null terminator if result string buffer was not filled.
    if (j < maxlen)
        v[j] = '\0';

    return true;
}

boolean send_check_reply(const char *send, const char *reply, uint32_t timeout, boolean multiline)
{
    if (!get_reply(send, timeout, multiline))
        return false;
    if (strlen(replybuffer) > strlen(reply))
        return (strstr(replybuffer, reply) != NULL);
    else
        return (strstr(reply, replybuffer) != NULL);
}

int available_uart()
{
    int length = 0;
#ifdef UART_ESP
    uart_get_buffered_data_len(UART_NUM, (size_t *)&length);
#else
    length = sw_any(serial_modem);
#endif
    return length;
}

void flush_input()
{
    if (available_uart() > 0)
    {
#ifdef UART_ESP
        uart_flush(UART_NUM);
#else
        sw_flush(serial_modem);
#endif
        //
    }
}

uint16_t get_reply(const char *send, uint32_t timeout, boolean multiline)
{
    uint16_t size_send = strlen(send);
    uint8_t timeout_count = 0;
    char *command = (char *)malloc((size_send + 2) * sizeof(char));
    if (command != NULL)
    {
        strcpy(command, send);
        if (send[size_send - 1] != '\r')
        {
            sprintf(command, "%s\r", send);
        }
        vTaskDelay(100 / portTICK_PERIOD_MS);
        uint16_t l = 0;
    sendmsj:
        flush_input();
        if (show_info)
            ESP_LOGI("SIM7000", " SENDING COMMAND: '%s'", command);
#ifdef UART_ESP
        uart_write_bytes(UART_NUM, command, strlen(command));
#else
        sw_write_bytes(serial_modem, (uint8_t *)command, strlen(command));
#endif
        vTaskDelay(200 / portTICK_PERIOD_MS);
        l = readline(timeout, multiline);
        if (l == TIMEOUT_MARK_AT)
            goto sendmsj;
        else if (l == TIMEOUT_MARK)
        {
            l = 0;
        }
        flush_input();
        free(command);
        return l;
    }
    return 0;
}

uint16_t readline(uint32_t timeout, boolean multiline)
{
    uint16_t respidx = 0;
    int length = available_uart();
    int total_length = 0;
    int start = esp_timer_get_time() / 1000;
    memset(replybuffer, 0, RX_BUF_SIZE);
    while (length == 0 && (esp_timer_get_time() / 1000 - start) < timeout)
    {
        length = available_uart();
        if (length > 0)
            break;
        vTaskDelay(50 / portTICK_PERIOD_MS);
    }
    if ((esp_timer_get_time() / 1000 - start) >= timeout && length == 0)
    {
        if (show_info)
            ESP_LOGI("SIM7000", "TIMEOUT readline %d", (int)(esp_timer_get_time() / 1000 - start));
        replybuffer[respidx] = 0;
        if ((esp_timer_get_time() / 1000 - last_success_response_time) >= MAX_TIME_TIMEOUT)
            (*ptr_wakeup_modem_sim7000)();
        return TIMEOUT_MARK;
    }
    last_success_response_time = esp_timer_get_time() / 1000;
    vTaskDelay(150 / portTICK_PERIOD_MS);
    length = available_uart();
    total_length = length;
    while (total_length < RX_BUF_SIZE && length > 0)
    {
#ifdef UART_ESP
        respidx += uart_read_bytes(UART_NUM, (uint8_t *)(replybuffer + respidx), length, timeout / portTICK_RATE_MS);
#else
        respidx += sw_read_bytes(serial_modem, (uint8_t *)(replybuffer + respidx), length, timeout);
#endif
        if (show_info)
            ESP_LOGI("SIM7000", "buffer: (total: %d) (respidx: %d) (length: %d)\n%s", total_length, respidx, length, replybuffer);
        vTaskDelay(150 / portTICK_PERIOD_MS);
        length = available_uart();
        total_length += length;
    }
    
    if (strstr(replybuffer, "SMSUB") != NULL)
    {
        memset(replybuffer_mqtt, 0, RX_BUF_SIZE);
        char *mqtt_msj = strstr(replybuffer, "SMSUB");
        strcpy(replybuffer_mqtt, mqtt_msj);
        check_mqtt_msj();
    }
    if (strstr(replybuffer, "NORMAL POWER DOWN") != NULL)
    {
        //TODO ENCENDER
    }
    return respidx;
    
}

// Send prefix, ", suffix, ", and newline.  Verify FONA response matches reply parameter.
boolean send_check_reply_quoted(char *prefix, char *suffix, char *reply, uint32_t timeout, boolean multiline)
{
    get_reply_quoted(prefix, suffix, timeout, false);
    return (strcmp(replybuffer, reply) == 0);
}

// Send prefix, ", suffix, ", and newline. Return response (and also set replybuffer with response).
uint16_t get_reply_quoted(char *prefix, char *suffix, uint32_t timeout, boolean multiline)
{
    char command[strlen(prefix) + strlen(suffix) + 3];
    sprintf(command, "%s\"%s\"", prefix, suffix);
    return get_reply(command, timeout, multiline);
}

uint8_t get_network_status(void)
{
    uint16_t status = 0;

    if (_type >= SIM7000A)
    {
        if (!send_parse_reply("AT+CGREG?", "+CGREG: ", &status, ',', 1, 1000, true))
        {
            return 0;
        }
    }
    else
    {
        if (!send_parse_reply("AT+CREG?", "+CREG: ", &status, ',', 1, 1000, true))
        {
            return 0;
        }
    }
    
    return status;
}

void connect_manually_operator()
{
    send_check_reply("AT+COPS=?", OK_REPLY, 50000, true);
    int start = esp_timer_get_time() / 1000;
    char operator[50] = "";
    while (!parse_reply_quoted("+COPS: ", &operator[0], 10, ',', 3) && esp_timer_get_time() / 1000 - start < 50000)
    {
        readline(50000, true);
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
    send_check_reply("AT+COPS=0", OK_REPLY, 130000, true);
    //}
    vTaskDelay(7000 / portTICK_PERIOD_MS); // Retry every 2s
    set_network_registration(2);
    vTaskDelay(7000 / portTICK_PERIOD_MS); // Retry every 2s
}

uint8_t get_RSSI(void)
{
    uint16_t reply;
    if (!send_parse_reply("AT+CSQ", "+CSQ: ", &reply, ',', 0, DEFAULT_TIMEOUT, true))
        return RSSI;
    RSSI = reply;
    return reply;
}
boolean set_network_registration(uint8_t n)
{
    char cmd[20] = "";
    sprintf(cmd, "AT+CGREG=%d", n);
    return send_check_reply(cmd, OK_REPLY, 3000, true);
}

uint8_t net_status()
{
    int n = get_network_status();
    ESP_LOGI("SIM7000", "Network status %d: ", n);
    if (n == 0)
        ESP_LOGI("SIM7000", "Not registered");
    if (n == 1)
        ESP_LOGI("SIM7000", "Registered (home)");
    if (n == 2)
        ESP_LOGI("SIM7000", "Not registered (searching)");
    if (n == 3)
        ESP_LOGI("SIM7000", "Denied");
    if (n == 4)
        ESP_LOGI("SIM7000", "Unknown");
    if (n == 5)
        ESP_LOGI("SIM7000", "Registered roaming");
    return n;
    
}

int8_t GPS_status(void)
{
    if (_type == SIM808_V2 || _type == SIM7000A || _type == SIM7000C || _type == SIM7000E || _type == SIM7000G)
    {
        // 808 V2 uses GNS commands and doesn't have an explicit 2D/3D fix status.
        // Instead just look for a fix and if found assume it's a 3D fix.
        get_reply("AT+CGNSINF", DEFAULT_TIMEOUT * 2, true);
        char *p = strstr(replybuffer, "+CGNSINF: ");
        if (p == 0)
            return -1;
        p += 10;
        //readline(DEFAULT_TIMEOUT, true); // eat 'OK'
        if (p[0] == '0')
            return 0; // GPS is not even on!

        p += 2; // Skip to second value, fix status.
        //DEBUG_PRINTLN(p);
        // Assume if the fix status is '1' then we have a 3D fix, otherwise no fix.
        if (p[0] == '1')
            return 3;
        else
            return 1;
    }
    if (_type == SIM5320A || _type == SIM5320E)
    {
        // FONA 3G doesn't have an explicit 2D/3D fix status.
        // Instead just look for a fix and if found assume it's a 3D fix.
        get_reply("AT+CGPSINFO", DEFAULT_TIMEOUT, true);
        char *p = strstr(replybuffer, "+CGPSINFO:");
        if (p == 0)
            return -1;
        if (p[10] != ',')
            return 3; // if you get anything, its 3D fix
        return 0;
    }
    else
    {
        // 808 V1 looks for specific 2D or 3D fix state.
        get_reply("AT+CGPSSTATUS?", DEFAULT_TIMEOUT, true);
        char *p = strstr(replybuffer, "SSTATUS: Location ");
        if (p == 0)
            return -1;
        p += 18;
        //readline(DEFAULT_TIMEOUT, false); // eat 'OK'
        //DEBUG_PRINTLN(p);
        if (p[0] == 'U')
            return 0;
        if (p[0] == 'N')
            return 1;
        if (p[0] == '2')
            return 2;
        if (p[0] == '3')
            return 3;
    }
    // else
    return 0;
}

uint8_t get_GPS_info(uint8_t arg, char *buffer, uint8_t maxbuff)
{
    //if(show_info)ESP_LOGI("SIM7000", "get_GPS_info _type: %d", _type);
    int32_t x = arg;

    if (_type == SIM5320A || _type == SIM5320E || _type == SIM7500A || _type == SIM7500E || _type == SIM7600A || _type == SIM7600C || _type == SIM7600E)
    {
        get_reply("AT+CGPSINFO", DEFAULT_TIMEOUT, true);
    }
    else if (_type == SIM808_V1)
    {
        char auxStr[21];
        sprintf(auxStr, "AT+CGPSINF=%d", x);
        get_reply(auxStr, DEFAULT_TIMEOUT, true);
    }
    else
    {
        get_reply("AT+CGNSINF", DEFAULT_TIMEOUT * 2, true);
    }

    char *p = strstr(replybuffer, "SINF");
    if (p == 0)
    {
        buffer[0] = 0;
        return 0;
    }

    p += 6;

    uint8_t len = fmax(maxbuff - 1, (int)strlen(p));
    strncpy(buffer, p, len);
    buffer[len] = 0;

    //readline(DEFAULT_TIMEOUT, true); // eat 'OK'
    return len;
}

/*boolean get_GPS(float *lat, float *lon, float *speed_kph, float *heading, float *altitude, 
uint16_t *year, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *min, float *sec,
uint8_t *fix_status, float *HDOP, uint8_t *number_sat, float *PDOP, float *VDOP)*/
boolean get_GPS(gps_info *info)
{

    char gpsbuffer[120];
    //if(show_info)ESP_LOGI("SIM7000", "get_GPS _type: %d", _type);
    // we need at least a 2D fix
    if (_type < SIM7000A)
    { // SIM7500 doesn't support AT+CGPSSTATUS? command
        //if(show_info)ESP_LOGI("SIM7000", "get_GPS _type (%d) < SIM7000A (%d)", _type, SIM7000A);
        if (GPS_status() < 2)
            return false;
    }

    // grab the mode 2^5 gps csv from the sim808
    uint8_t res_len = get_GPS_info(32, gpsbuffer, 120);

    // make sure we have a response
    if (res_len == 0)
        return false;

    if (_type == SIM808_V2 || _type == SIM7000A || _type == SIM7000C || _type == SIM7000E || _type == SIM7000G ||
        _type == SIM7500A || _type == SIM7500E || _type == SIM7600A || _type == SIM7600C || _type == SIM7600E)
    {
        // Parse 808 V2 response.  See table 2-3 from here for format:
        // http://www.adafruit.com/datasheets/SIM800%20Series_GNSS_Application%20Note%20V1.00.pdf

        // skip GPS run status
        char *tok = strtok(gpsbuffer, ",");
        if (!tok)
            return false;

        tok = strtok(NULL, ",");

        if (!tok)
            return false;
        info->fix_status = atoi(tok);

        char *date = strtok(NULL, ",");
        if (!date)
            return false;

        // Seconds
        char *ptr = date + 12;
        info->second = atof(ptr);

        // Minutes
        ptr[0] = 0;
        ptr = date + 10;
        info->minute = atoi(ptr);

        // Hours
        ptr[0] = 0;
        ptr = date + 8;
        info->hour = atoi(ptr);

        // Day
        ptr[0] = 0;
        ptr = date + 6;
        info->day = atoi(ptr);

        // Month
        ptr[0] = 0;
        ptr = date + 4;
        info->month = atoi(ptr);

        // Year
        ptr[0] = 0;
        ptr = date;
        info->year = atoi(ptr);

        // grab the latitude
        char *latp = strtok(NULL, ",");
        if (!latp)
            return false;

        // grab longitude
        char *longp = strtok(NULL, ",");
        if (!longp)
            return false;

        info->latitude = atof(latp);
        info->longitude = atof(longp);

        char *altp = strtok(NULL, ",");

        // grab altitude
        if (!altp)
            return false;
        info->altitude = atof(altp);

        char *speedp = strtok(NULL, ",");

        // grab the speed in km/h
        if (!speedp)
            return false;
        info->speed_kph = atof(speedp);

        char *coursep = strtok(NULL, ",");

        // grab the speed in knots
        if (!coursep)
            return false;
        info->heading = atof(coursep);

        // skip Fix Mode
        tok = strtok(NULL, ",");
        if (!tok)
            return false;

        // skip Reserved1
        // tok = strtok(NULL, ",");

        char *hdopp = strtok(NULL, ",");

        if (!hdopp)
            return false;
        info->HDOP = atof(hdopp);

        // PDOP
        tok = strtok(NULL, ",");

        if (!tok)
            return false;
        info->PDOP = atof(tok);

        // VDOP
        tok = strtok(NULL, ",");
        if (!tok)
            return false;
        info->VDOP = atof(tok);

        // skip Reserved2
        // tok = strtok(NULL, ",");

        //  GPS Satellites in View
        tok = strtok(NULL, ",");
        if (!tok)
            return false;
        char *numbersatp = strtok(NULL, ",");
        // only grab number_sat if needed

        if (!numbersatp)
            return false;
        info->number_sats = atoi(numbersatp);
    }

    return true;
}

boolean MQTT_set_parameter(const char *paramTag, const char *paramValue, uint16_t port)
{
    char cmdStr[50];

    if (strcmp(paramTag, "CLIENTID") == 0 || strcmp(paramTag, "URL") == 0 || strcmp(paramTag, "TOPIC") == 0 || strcmp(paramTag, "MESSAGE") == 0)
    {
        if (port == 0)
            sprintf(cmdStr, "AT+SMCONF=\"%s\",\"%s\"", paramTag, paramValue); // Quoted paramValue
        else
            sprintf(cmdStr, "AT+SMCONF=\"%s\",\"%s\",\"%i\"", paramTag, paramValue, port);
        if (!send_check_reply(cmdStr, OK_REPLY, DEFAULT_TIMEOUT, true))
            return false;
    }
    else
    {
        sprintf(cmdStr, "AT+SMCONF=\"%s\",\"%s\"", paramTag, paramValue); // Unquoted paramValue
        if (!send_check_reply(cmdStr, OK_REPLY, DEFAULT_TIMEOUT, true))
            return false;
    }

    return true;
}
// Connect or disconnect MQTT
boolean MQTT_connect(boolean yesno)
{
    if (yesno)
        return send_check_reply("AT+SMCONN", OK_REPLY, 30000, true);
    else
        return send_check_reply("AT+SMDISC", OK_REPLY, DEFAULT_TIMEOUT, true);
}

// Query MQTT connection status
boolean MQTT_connection_status(void)
{

    if (!send_check_reply("AT+SMSTATE?", "+SMSTATE: 1", DEFAULT_TIMEOUT, true))
        return false;
    return true;
}

// Subscribe to specified MQTT topic
// QoS can be from 0-2
boolean MQTT_subscribe(const char *topic, uint8_t QoS)
{
    char cmdStr[70];
    sprintf(cmdStr, "AT+SMSUB=\"%s\",%i", topic, QoS);

    if (!send_check_reply(cmdStr, OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    return true;
}

// Unsubscribe from specified MQTT topic
boolean MQTT_unsubscribe(const char *topic)
{
    char cmdStr[70];
    sprintf(cmdStr, "AT+SMUNSUB=\"%s\"", topic);
    if (!send_check_reply(cmdStr, OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    return true;
}

// Publish to specified topic
// Message length can be from 0-512 bytes
// QoS can be from 0-2
// Server hold message flag can be 0 or 1
boolean MQTT_publish(const char *topic, const char *message, uint16_t contentLength, uint8_t QoS, uint8_t retain)
{
    char cmdStr[150];
    sprintf(cmdStr, "AT+SMPUB=\"%s\",%i,%i,%i", topic, contentLength, QoS, retain);

    get_reply(cmdStr, 20000, true);
    if (strstr(replybuffer, ">") == NULL)
        return false; // Wait for "> " to send message
    if (!send_check_reply(message, message, 15000, true))
    {
        if (strstr(replybuffer, "OK") != NULL)
            return true;
        return false; // Now send the message
    }
    return true;
}

// Change MQTT data format to hex
// Enter "true" if you want hex, "false" if you don't
boolean MQTT_dataFormatHex(boolean yesno)
{
    char cmdStr[40];
    sprintf(cmdStr, "AT+SMPUBHEX=%d", yesno);
    return send_check_reply(cmdStr, OK_REPLY, 5000, true);
}

void set_callback_mqtt_msj(void (*ptr_c)(char *, char *))
{
    ptr_callback_mqtt_msj = ptr_c;
}

void check_mqtt_msj()
{
    if (strstr(replybuffer_mqtt, "SMSUB:") != NULL && strchr(replybuffer_mqtt, '{') != NULL && strchr(replybuffer_mqtt, '}') != NULL)
    {
        //+SMSUB: "centrack/command/50291AEC0E0","{"code":1,"val":1}"
        //+SMSUB: "centrack/command/50291AEC0E0","{"code":1,"msj":[{"name":"apn","val":"internet.movistar.cr"},{"name":"user","val":"movistarcr"},{"name":"pwd","val":"movistarcr"}]}"
        ESP_LOGI("SIM7000", "*** Received MQTT message! *** (%d)", strlen(replybuffer_mqtt));

        ESP_LOGI("SIM7000", "|%s|->(%d)", replybuffer_mqtt, strlen(replybuffer_mqtt));
        char *p = strstr(replybuffer_mqtt, "SMSUB:");
        if (p != NULL)
        {
            p += 8;
            char *topic_p = strtok(p, "\",\"");
            char *topic = (char *)malloc(strlen(topic_p) + 1);

            if (topic != NULL)
            {
                strcpy(topic, topic_p);
                char *message_p = p + strlen(topic_p) + 3;
                message_p = strchr(message_p, '{');
                if (message_p != NULL)
                {
                    size_t size_msj = strlen(message_p) + 2;
                    char *msj = (char *)malloc(size_msj);
                    if (msj != NULL && message_p != NULL && size_msj > 2)
                    {
                        memset(msj, 0, size_msj);
                        char *line_change = strchr(message_p, '\n');
                        if (line_change != NULL)
                            line_change[0] = 0;
                        char *end_p = strrchr(message_p, '}');
                        if (end_p != NULL)
                        {
                            strncpy(msj, message_p, (end_p - message_p) + 1);
                            if (show_info)
                                ESP_LOGI("SIM7000", "Topic: %s", topic);
                            if (show_info)
                                ESP_LOGI("SIM7000", "Message: %s", msj);
                            if (strcmp(last_replybuffer_mqtt, msj) != 0 || *last_hash_mqtt_msj_sim_ptr == 0)
                                (*ptr_callback_mqtt_msj)(topic, msj);
                            else
                                (*ptr_add_confirm_mqtt_msj)();
                            memset(last_replybuffer_mqtt, 0, RX_BUF_SIZE);
                            strcpy(last_replybuffer_mqtt, msj);
                        }
                        free(msj);
                    }
                }
                free(topic);
            }
        }
    }
}

/********* HTTP LOW LEVEL FUNCTIONS  ************************************/

boolean HTTP_init()
{
    return send_check_reply("AT+HTTPINIT", OK_REPLY, DEFAULT_TIMEOUT, true);
}

boolean HTTP_term()
{
    return send_check_reply("AT+HTTPTERM", OK_REPLY, DEFAULT_TIMEOUT, true);
}

void HTTP_para_start(char *parameter, boolean quoted)
{
    vTaskDelay(300 / portTICK_PERIOD_MS);
    flush_input();
    char buff[40];
    if (quoted)
        sprintf(buff, "AT+HTTPPARA=\"%s\",\"", parameter);
    else
        sprintf(buff, "AT+HTTPPARA=\"%s\",", parameter);
    if (show_info)
        ESP_LOGI("SIM7000", "\t---> %s", buff);
#ifdef UART_ESP
    uart_write_bytes(UART_NUM, buff, strlen(buff));
#else
    sw_write_bytes(serial_modem, (uint8_t *)buff, strlen(buff));
#endif
}

boolean HTTP_para_end(boolean quoted)
{
    if (quoted)
#ifdef UART_ESP
        uart_write_bytes(UART_NUM, "\"\r", 2);
#else
        sw_write_bytes(serial_modem, (uint8_t *)"\"\r", 2);
#endif
    else
#ifdef UART_ESP
        uart_write_bytes(UART_NUM, "\r", 1);
#else
        sw_write_bytes(serial_modem, (uint8_t *)"\r", 1);
#endif
    if (show_info)
        ESP_LOGI("SIM7000", "%s", quoted ? "\"" : " ");
    return expect_reply(OK_REPLY, 10000);
}

boolean HTTP_para(char *parameter, const char *value)
{
    HTTP_para_start(parameter, true);
    if (show_info)
        ESP_LOGI("SIM7000", "%s", value);
#ifdef UART_ESP
    uart_write_bytes(UART_NUM, value, strlen(value));
#else
    sw_write_bytes(serial_modem, (uint8_t *)value, strlen(value));
#endif
    return HTTP_para_end(true);
}

boolean HTTP_para_number(char *parameter, int32_t value)
{
    HTTP_para_start(parameter, false);
    char number[10];
    sprintf(number, "%d", value);
    if (show_info)
        ESP_LOGI("SIM7000", "%s", number);
#ifdef UART_ESP
    uart_write_bytes(UART_NUM, number, strlen(number));
#else
    sw_write_bytes(serial_modem, (uint8_t *)number, strlen(number));
#endif
    return HTTP_para_end(false);
}

boolean HTTP_data(uint32_t size, uint32_t maxTime)
{
    flush_input();
    if (show_info)
        ESP_LOGI("SIM7000", "\t---> AT+HTTPDATA=%d,%d", size, maxTime);
    char command[40];
    sprintf(command, "AT+HTTPDATA=%d,%d", size, maxTime);
    return send_check_reply(command, "DOWNLOAD", maxTime, true);
}

boolean HTTP_action(uint8_t method, uint16_t *status, uint16_t *datalen, int32_t timeout)
{
    // Send request.
    *status = 404;
    char command[40];
    sprintf(command, "AT+HTTPACTION=%d", method);
    if (!send_check_reply(command, OK_REPLY, 10000, true))
        return false;
    // Parse response status and size.
    readline(timeout, true);
    if (!parse_reply("+HTTPACTION:", status, ',', 1))
        return false;
    if (!parse_reply("+HTTPACTION:", datalen, ',', 2))
        return false;
    return true;
}

boolean HTTP_readall(uint16_t *datalen)
{
    *datalen = 0;
    int start = esp_timer_get_time() / 1000;
    while ((esp_timer_get_time() / 1000 - start) < 25000 && *datalen == 0)
    {
        get_reply("AT+HTTPREAD", 25000, true);
        if (!parse_reply("+HTTPREAD:", datalen, ',', 0))
        {
            if (!parse_reply("+HTTPACTION:", datalen, ',', 2))
            {
                return false;
            }
        }
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
    return true;
}

boolean HTTP_ssl(boolean onoff)
{
    if (_type > SIM7000G)
    {
        char command[40];
        sprintf(command, "AT+HTTPSSL=%d", onoff ? 1 : 0);
        return send_check_reply(command, OK_REPLY, 10000, true);
    }
    else
    {
        return true;
    }
}

boolean HTTP_STATUS(uint16_t *status, uint16_t *datalen)
{
    if (!send_check_reply("AT+HTTPSTATUS?", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    // Parse response status and size.
    // readline(DEFAULT_TIMEOUT, true);
    parse_reply("+HTTPSTATUS:", status, ',', 1);
    parse_reply("+HTTPSTATUS:", datalen, ',', 2);
    if (*datalen == 0)
    {
        parse_reply("+HTTPACTION:", status, ',', 1);
        parse_reply("+HTTPACTION:", datalen, ',', 2);
    }
    return true;
}

/********* HTTP HIGH LEVEL FUNCTIONS ***************************/

boolean HTTP_GET_start(char *url, uint16_t *status, uint16_t *datalen)
{
    if (!HTTP_setup(url))
        return false;
    // HTTP GET
    if (!HTTP_action(SIM7000_HTTP_GET, status, datalen, 30000))
        return false;
    int start = esp_timer_get_time() / 1000;
    while (*status != 200 && (esp_timer_get_time() / 1000 - start) < 15000)
    {
        HTTP_STATUS(status, datalen);
        if (show_info)
            ESP_LOGI("SIM7000", "Status: %d  Len: %d", *status, *datalen);
        if (*status == 0)
            break;
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    if (!HTTP_readall(datalen))
        return false;

    return true;
}

void HTTP_GET_end(void)
{
    HTTP_term();
}

boolean HTTP_POST_start(char *url, char *contenttype, const uint8_t *postdata, uint16_t postdatalen, uint16_t *status, uint16_t *datalen)
{
    if (!HTTP_setup(url))
        return false;

    if (!HTTP_para("CONTENT", contenttype))
    {
        return false;
    }

    // if (!HTTP_para("USERDATA", (char *)postdata))
    // {
    //     return false;
    // }
    // HTTP POST data
    if (!HTTP_data(postdatalen, postdatalen * 100))
        return false;
// vTaskDelay(50 / portTICK_PERIOD_MS);
#ifdef UART_ESP
    uart_write_bytes(UART_NUM, (char *)postdata, postdatalen);
#else
    sw_write_bytes(serial_modem, postdata, postdatalen);
#endif
    // vTaskDelay(50 / portTICK_PERIOD_MS); //*100
    if (!expect_reply(OK_REPLY, 10000))
        return false;
    vTaskDelay(150 / portTICK_PERIOD_MS);
    // HTTP POST
    if (!HTTP_action(SIM7000_HTTP_POST, status, datalen, 30000))
        return false;
    vTaskDelay(150 / portTICK_PERIOD_MS);
    int start = esp_timer_get_time() / 1000;
    while (*status != 200 && (esp_timer_get_time() / 1000 - start) < 5000)
    {
        HTTP_STATUS(status, datalen);
        if (show_info)
            ESP_LOGI("SIM7000", "Status: %d  Len: %d", *status, *datalen);
        if (*status == 0)
            break;
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }

    // HTTP response data
    if (!HTTP_readall(datalen))
        return false;

    return true;
}

void HTTP_POST_end(void)
{
    HTTP_term();
}

void set_user_agent(char *puseragent)
{
    useragent = puseragent;
}

/********* HTTP HELPERS ****************************************/

boolean HTTP_setup(char *url)
{
    // Handle any pending
    HTTP_term();

    // Initialize and set parameters
    if (!HTTP_init())
        return false;
    if (!HTTP_para_number("CID", 1))
        return false;
    // if (!HTTP_para("UA", useragent))
    //     return false;
    if (!HTTP_para("URL", url))
        return false;

    // HTTPS redirect
    if (httpsredirect)
    {
        if (!HTTP_para_number("REDIR", 1))
            return false;

        if (!HTTP_ssl(true))
            return false;
    }

    return true;
}

boolean expect_reply(char *reply, uint32_t timeout)
{
    boolean resp = false;
    int start = esp_timer_get_time() / 1000;
    while (!resp && (esp_timer_get_time() / 1000 - start) < timeout)
    {
        readline(timeout, true);
        if (show_info)
            ESP_LOGI("SIM7000", "expect_reply -> reply: %s       timeout %d    replybuffer: %s", reply, (int)(esp_timer_get_time() / 1000 - start), replybuffer);
        resp = (strstr(replybuffer, reply) != NULL);
        if (!resp)
            vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    return resp;
}

/********* SMS **********************************************************/

uint8_t getSMSInterrupt(void)
{
    uint16_t reply;

    if (!send_parse_reply("AT+CFGRI?", "+CFGRI: ", &reply, ',', 0, DEFAULT_TIMEOUT, true))
        return 0;

    return reply;
}

boolean setSMSInterrupt(uint8_t i)
{
    char buff[15];
    sprintf(buff, "AT+CFGRI=%d", i);
    return send_check_reply(buff, OK_REPLY, DEFAULT_TIMEOUT, true);
}

int8_t getNumSMS(void)
{
    uint16_t numsms;
    // get into text mode
    if (!send_check_reply("AT+CMGF=1", OK_REPLY, DEFAULT_TIMEOUT, true))
        return -1;
    // ask how many sms are stored
    if (send_parse_reply("AT+CPMS?", SIM7000_PREF_SMS_STORAGE ",", &numsms, ',', 0, DEFAULT_TIMEOUT, true))
        return numsms;
    if (send_parse_reply("AT+CPMS?", "\"SM\",", &numsms, ',', 0, DEFAULT_TIMEOUT, true))
        return numsms;
    if (send_parse_reply("AT+CPMS?", "\"SM_P\",", &numsms, ',', 0, DEFAULT_TIMEOUT, true))
        return numsms;
    return -1;
}

// Reading SMS's is a bit involved so we don't use helpers that may cause delays or debug
// printouts!
boolean readSMS(uint8_t i, char *smsbuff, uint16_t maxlen, uint16_t *readlen)
{
    // text mode
    if (!send_check_reply("AT+CMGF=1", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    // Select memory storages
    if (!send_check_reply("AT+CPMS=\"SM\",\"SM\",\"SM\"", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    // Set new message indications to TE.
    //if (!send_check_reply("AT+CNMI=2,1", OK_REPLY, DEFAULT_TIMEOUT, true))
    //    return false;

    // show all text mode parameters
    if (!send_check_reply("AT+CSDH=1", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    uint16_t thesmslen = 0;
    char buff[25];
    sprintf(buff, "AT+CMGR=%d", i);
    if (show_info)
        ESP_LOGI("SIM7000", "%s", buff);
    get_reply(buff, 10000, true); //  do not print debug!
    //ESP_LOGW("SIM7000", "SMS:\n|%s|", replybuffer);
    if (!parse_reply("+CMGR:", &thesmslen, ',', 11))
    {
        *readlen = 0;
        return false;
    }
    *readlen = thesmslen;
    //PARA EVITAR QUE HAYA RUIDO EN EL MENSAJE (MQTT SUB)
    char *msj_cmgr = strstr(replybuffer, "+CMGR:");
    char *msj = strchr(msj_cmgr, '\n');
    if (maxlen > thesmslen && maxlen > strlen(msj))
    {
        strncpy(smsbuff, msj + 1, thesmslen);
        smsbuff[thesmslen] = 0;
    }
    return true;
}

// Retrieve the sender of the specified SMS message and copy it as a string to
// the sender buffer.  Up to senderlen characters of the sender will be copied
// and a null terminator will be added if less than senderlen charactesr are
// copied to the result.  Returns true if a result was successfully retrieved,
// otherwise false.
boolean getSMSSender(uint8_t i, char *sender, int senderlen)
{
    // Ensure text mode and all text mode parameters are sent.
    if (!send_check_reply("AT+CMGF=1", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    if (!send_check_reply("AT+CSDH=1", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    //ESP_LOGW("SIM7000", "CSDH:|%s|", replybuffer);
    char buff[25];
    sprintf(buff, "AT+CMGR=%d", i);
    if (show_info)
        ESP_LOGI("SIM7000", "%s", buff);
    // Send command to retrieve SMS message and parse a line of response.
    get_reply(buff, 10000, true); //  do not print debug!
    if (show_info)
        ESP_LOGI("SIM7000", "%s", replybuffer);
    //ESP_LOGW("SIM7000", "CMGR:|%s|", replybuffer);
    // Parse the second field in the response.
    boolean result = parse_reply_quoted("+CMGR:", sender, senderlen, ',', 1);
    // Drop any remaining data from the response.
    flush_input();
    return result;
}

boolean sendSMS(const char *smsaddr, const char *smsmsg)
{
    if (!send_check_reply("AT+CMGF=1", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;

    char sendcmd[200] = "";
    sprintf(sendcmd, "AT+CMGS=\"%s\"", smsaddr);
    if (!send_check_reply(sendcmd, "> ", DEFAULT_TIMEOUT, true))
        return false;

    if (show_info)
        ESP_LOGI("SIM7000", "> %s", smsmsg);

    // no need for extra NEWLINE characters mySerial->println(smsmsg);
    // no need for extra NEWLINE characters mySerial->println();
    char buff_send[strlen(smsmsg) + 2];
    memset(buff_send, 0, strlen(smsmsg) + 2);
    strcpy(buff_send, smsmsg);
    buff_send[strlen(smsmsg)] = 0x1A;
    if (!send_check_reply(buff_send, OK_REPLY, 60000, true))
        return false;
    ESP_LOGW("SIM7000", "MSG SEND: (%s)\nRESP: (%s)", buff_send, replybuffer);
    return true;
}

boolean deleteSMS(uint8_t i)
{
    if (!send_check_reply("AT+CMGF=1", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    // delete an sms
    char sendbuff[15] = "";
    sprintf(sendbuff, "AT+CMGD=%d", i);
    return send_check_reply(sendbuff, OK_REPLY, 5000, true);
}

boolean deleteAllSMS()
{
    if (!send_check_reply("AT+CMGF=1", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    return send_check_reply("AT+CMGD=1,2", OK_REPLY, 20000, true);
}

boolean enable_GPRS(boolean onoff)
{
    if (_type == SIM5320A || _type == SIM5320E || _type == SIM7500A || _type == SIM7500E || _type == SIM7600A || _type == SIM7600C || _type == SIM7600E)
    {
        if (onoff)
        {
            // disconnect all sockets
            send_check_reply("AT+CIPSHUT", "SHUT OK", 5000, true);

            if (!send_check_reply("AT+CGATT=1", OK_REPLY, 10000, true))
                return false;

            // set bearer profile access point name
            if (apn)
            {
                // Send command AT+CGSOCKCONT=1,"IP","<apn value>" where <apn value> is the configured APN name.
                if (!send_check_reply_quoted("AT+CGSOCKCONT=1,\"IP\",", apn, OK_REPLY, 10000, true))
                    return false;

                // set username/password
                if (apnusername)
                {
                    char authstring[100] = "AT+CGAUTH=1,1,\"";
                    // char authstring[100] = "AT+CSOCKAUTH=1,1,\""; // For 3G
                    char *strp = authstring + strlen(authstring);
                    strcpy(strp, apnusername);
                    strp += strlen(apnusername);
                    strp[0] = '\"';
                    strp++;
                    strp[0] = 0;

                    if (apnpassword)
                    {
                        strp[0] = ',';
                        strp++;
                        strp[0] = '\"';
                        strp++;
                        strcpy(strp, apnpassword);
                        strp += strlen(apnpassword);
                        strp[0] = '\"';
                        strp++;
                        strp[0] = 0;
                    }

                    if (!send_check_reply(authstring, OK_REPLY, 10000, true))
                        return false;
                }
            }

            // connect in transparent mode
            if (!send_check_reply("AT+CIPMODE=1", OK_REPLY, 10000, true))
                return false;
            // open network
            if (_type == SIM5320A || _type == SIM5320E)
            {
                if (!send_check_reply("AT+NETOPEN=,,1", "Network opened", 10000, true))
                    return false;
            }
            else if (_type == SIM7500A || _type == SIM7500E || _type == SIM7600A || _type == SIM7600C || _type == SIM7600E)
            {
                if (!send_check_reply("AT+NETOPEN", OK_REPLY, 10000, true))
                    return false;
            }
            //readline(DEFAULT_TIMEOUT, true); // eat 'OK'
        }
        else
        {
            // close GPRS context
            if (_type == SIM5320A || _type == SIM5320E)
            {
                if (!send_check_reply("AT+NETCLOSE", "Network closed", 10000, true))
                    return false;
            }
            else if (_type == SIM7500A || _type == SIM7500E || _type == SIM7600A || _type == SIM7600C || _type == SIM7600E)
            {
                get_reply("AT+NETCLOSE", DEFAULT_TIMEOUT, true);
                get_reply("AT+CHTTPSSTOP", DEFAULT_TIMEOUT, true);
            }

            //readline(DEFAULT_TIMEOUT, true); // eat 'OK'
        }
    }
    else
    {
        if (onoff)
        {
            // if (_type < SIM7000A) { // UNCOMMENT FOR LTE ONLY!
            // disconnect all sockets
            send_check_reply("AT+CIPSHUT", "SHUT OK", 20000, true);

            if (!send_check_reply("AT+CGATT=1", OK_REPLY, 10000, true))
                return false;

            // set bearer profile! connection type GPRS
            //send_check_reply("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"", OK_REPLY, 10000, true);

            // } // UNCOMMENT FOR LTE ONLY!

            vTaskDelay(500 / portTICK_PERIOD_MS);
            //delay(200); // This seems to help the next line run the first time

            // set bearer profile access point name
            if (apn)
            {
                char cmdBuff[100];
                sprintf(cmdBuff, "AT+SAPBR=3,1,\"APN\",\"%s\"", apn);
                send_check_reply(cmdBuff, OK_REPLY, 3000, false);
                sprintf(cmdBuff, "AT+SAPBR=3,1,\"USER\",\"%s\"", apnusername);
                send_check_reply(cmdBuff, OK_REPLY, 3000, false);
                sprintf(cmdBuff, "AT+SAPBR=3,1,\"PWD\",\"%s\"", apnpassword);
                send_check_reply(cmdBuff, OK_REPLY, 3000, false);
            }
            // open bearer
            send_check_reply("AT+SAPBR=1,1", OK_REPLY, 5000, false);

            // if (_type < SIM7000A) { // UNCOMMENT FOR LTE ONLY!
            // bring up wireless connection
            vTaskDelay(500 / portTICK_PERIOD_MS);
            if (!send_check_reply("AT+CIICR", OK_REPLY, 85000, false))
                return false;
            // } // UNCOMMENT FOR LTE ONLY!
        }
        else
        {
            // disconnect all sockets
            if (!send_check_reply("AT+CIPSHUT", "SHUT OK", 20000, true))
                return false;

            // close bearer
            if (!send_check_reply("AT+SAPBR=0,1", OK_REPLY, 10000, true))
                return false;

            // if (_type < SIM7000A) { // UNCOMMENT FOR LTE ONLY!
            if (!send_check_reply("AT+CGATT=0", OK_REPLY, 10000, true))
                return false;
            // } // UNCOMMENT FOR LTE ONLY!
        }
    }
    return true;
}

boolean enableNTP()
{
    if (!send_check_reply("AT+CNTP=\"0.cr.pool.ntp.org\",-24", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    if (!send_check_reply("AT+CNTP", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    return true;
}

boolean setNetworkTime()
{
    if (!send_check_reply("AT+CNTP=\"0.cr.pool.ntp.org\",-24", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    uint16_t v = 0;
    int start = esp_timer_get_time() / 1000;
    while (v != 1 && esp_timer_get_time() / 1000 - start < 3000)
    {
        send_parse_reply("AT+CNTP", "+CNTP:", &v, ',', 0, DEFAULT_TIMEOUT, true);
        vTaskDelay(DEFAULT_TIMEOUT / portTICK_PERIOD_MS);
    }
    if (!send_check_reply("AT+CCLK?", OK_REPLY, DEFAULT_TIMEOUT, true))
        return false;
    char time[25] = "";
    char zzst[5] = "";
    boolean result = parse_reply_quoted("+CCLK:", time, 25, '-', 0);
    if (result)
    {
        parse_reply_quoted("+CCLK:", zzst, 5, '-', 1);
        zz = atoi(zzst) * -1;
    }
    else
    {
        parse_reply_quoted("+CCLK:", time, 25, '+', 0);
        parse_reply_quoted("+CCLK:", zzst, 5, '+', 1);
        zz = atoi(zzst);
    }

    if (show_info)
        ESP_LOGI("SIM7000", "TIME %s", time);
    struct tm tm;
    memset(&tm, 0, sizeof(tm));
    strptime(time, "%y/%m/%d,%H:%M:%S", &tm);
    time_t time_of_day;
    time_of_day = mktime(&tm);
    struct timeval now = {.tv_sec = (time_t)((int)time_of_day)};
    settimeofday(&now, NULL);
    if (show_info)
        ESP_LOGI("SIM7000", "seconds since the Epoch: %d", (int)now.tv_sec);
    return true;
}