#ifndef H__MODEM_CONTROL__H
#define H__MODEM_CONTROL__H
#include "esp_timer.h"
#include "driver/gpio.h"
#include "sim7000.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"


#define SIM7000_RI (39)
#define SIM7000_STATUS (38)

#define PWRKEY (18)
#define NRESET (15)
#define MODEM_POWER (12)


#define MODEM_ACTIVE_TIME_MS 14000

#define ENABLE_GPS_TIMEOUT 30000

#define CONNECTING_CELL_NETWORK_TIMEOUT 400000
#define OPENING_WIRELESS_CONNECTION_TIMEOUT 150000

char APN[64];
char USER[32];
char PWD[32];

boolean IR_status;

esp_err_t _init_gpios();
void turn_off_modem();
esp_err_t  turn_on_modem();
esp_err_t  reset_modem();


void delay_start_up();
esp_err_t start_up_sim(uint8_t force_conn);
int8_t is_sim_connected(boolean fast_process);

void (*restart_modem_ptr)();
void (*power_modem_ptr)();

void delay_start_up();

#endif