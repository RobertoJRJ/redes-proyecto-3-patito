#ifndef _SIM7000_H
#define _SIM7000_H
#include "esp_err.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp32/rom/ets_sys.h"
#include "esp_timer.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "math.h"
#include "sys/time.h"
#include "config.h"
#define UART_ESP
#ifdef UART_ESP
#include "soc/uart_struct.h"
#include "driver/uart.h"
#else
#include "sw_serial.h"
#endif
static const char *TAG_SIM = "SIM7000";

#define OK_REPLY "OK"
#define DEFAULT_TIMEOUT 500

#define SIM800L 1
#define SIM800H 6

#define SIM808_V1 2
#define SIM808_V2 3

#define SIM5320A 4
#define SIM5320E 5

#define SIM7000A 7
#define SIM7000C 8
#define SIM7000E 9
#define SIM7000G 10

#define SIM7500A 11
#define SIM7500E 12

#define SIM7600A 13
#define SIM7600C 14
#define SIM7600E 15

#define TIMEOUT_MARK 9998
#define TIMEOUT_MARK_AT 9999
// Set the preferred SMS storage.
//   Use "SM" for storage on the SIM.
//   Use "ME" for internal storage on the FONA chip
#define SIM7000_PREF_SMS_STORAGE "\"SM\""
//#define SIM7000_PREF_SMS_STORAGE "\"ME\""

#define SIM7000_HTTP_GET 0
#define SIM7000_HTTP_POST 1
#define SIM7000_HTTP_HEAD 2

#define TXD_MODEM (22)
#define RXD_MODEM (23)
#define RTS_MODEM (-1)
#define CTS_MODEM (-1)
//#define PWR   (0)
#ifdef UART_ESP
#define UART_NUM UART_NUM_2
#else
SwSerial *serial_modem;
#endif
#define false 0
#define true 1
typedef uint8_t boolean;

//2 KB
#define RX_BUF_SIZE 2048

typedef struct
{
    double latitude, longitude;                                   //16
    float speed_kph, heading, altitude, HDOP, PDOP, VDOP;         //24
    uint64_t mileage;                                             //8
    uint16_t year;                                                //2
    uint8_t month, day, hour, minute, second;                     //5
    uint8_t fix_status, number_sats, is_stop;                     //3
} gps_info;  

int zz;
char IMEI[25];
uint8_t RSSI;
char replybuffer[RX_BUF_SIZE];
char replybuffer_mqtt[RX_BUF_SIZE];
char last_replybuffer_mqtt[RX_BUF_SIZE];
uint8_t _type;
char *apn;
char *apnusername;
char *apnpassword;
boolean httpsredirect;
char *useragent;
boolean firstTime;
boolean GPS_modem_state;

long last_success_response_time;
#define MAX_TIME_TIMEOUT 40000

int *last_hash_mqtt_msj_sim_ptr;

boolean (*ptr_wakeup_modem_sim7000)();

void (*ptr_add_confirm_mqtt_msj)();
void (*ptr_callback_mqtt_msj)(char *, char *);
void set_callback_mqtt_msj(void (*ptr_c)(char *, char *));

esp_err_t start_uart();
boolean is_there_a_sim(int timeout);
boolean sincronize_at(int16_t timeout);
esp_err_t initialize_sim7000_uart();
boolean set_local_data_flow_control(uint8_t dce_by_dte, uint8_t dte_by_dce);
boolean set_report_mobile_equipment_error(uint8_t error_lvl);
boolean set_functionality(uint8_t option, uint8_t reset_MT);
boolean set_preferred_mode(uint8_t mode);
boolean set_preferred_LTE_mode(uint8_t mode);
boolean set_operating_band(const char *mode, uint8_t band);
void set_network_settings(char *papn, char *pusername, char *ppassword);
void set_HTTPS_redirect(boolean onoff);
boolean indicate_RI_using_URC(uint8_t mode);
boolean enable_RTC(boolean i);
boolean enable_sleep_mode(boolean onoff);
boolean set_eDRX(uint8_t mode, uint8_t connType, char *eDRX_val);
boolean enable_PSM_TAU(boolean onoff, char *TAU_val, char *activeTime_val);
boolean enable_PSM(boolean onoff);
boolean read_PSM_dynamic_parameters();
boolean GPS_work_mode_set(uint8_t gps_mode, uint8_t glo_mode, uint8_t bd_mode, uint8_t gal_mode);
boolean enable_GPS(boolean onoff);
boolean wireless_conn_status(void);
boolean open_wireless_connection(boolean onoff);
boolean send_parse_reply(char *tosend, char *toreply, uint16_t *v, char divider, uint8_t index, uint32_t timeout, boolean multiline);
boolean parse_reply(char *toreply, uint16_t *v, char divider, uint8_t index);
boolean parse_reply_quoted(char *toreply, char *v, int maxlen, char divider, uint8_t index);
boolean send_check_reply(const char *send, const char *reply, uint32_t timeout, boolean multiline);
int available_uart();
void flush_input();
uint16_t get_reply(const char *send, uint32_t timeout, boolean multiline);
uint16_t readline(uint32_t timeout, boolean multiline);
boolean send_check_reply_quoted(char *prefix, char *suffix, char *reply, uint32_t timeout, boolean multiline);
uint16_t get_reply_quoted(char *prefix, char *suffix, uint32_t timeout, boolean multiline);
uint8_t get_network_status(void);
boolean reset_functionality();
void connect_manually_operator();
uint8_t get_RSSI(void);
boolean set_network_registration(uint8_t n);
uint8_t net_status();
int8_t GPS_status(void);
uint8_t get_GPS_info(uint8_t arg, char *buffer, uint8_t maxbuff);
//boolean get_GPS(float *lat, float *lon, float *speed_kph, float *heading, float *altitude,
//uint16_t *year, uint8_t *month, uint8_t *day, uint8_t *hour, uint8_t *min, float *sec,
//uint8_t *fix_status, float *HDOP, uint8_t *number_sat, float *PDOP, float *VDOP);
boolean get_GPS(gps_info *info);
boolean MQTT_set_parameter(const char *paramTag, const char *paramValue, uint16_t port);

boolean MQTT_connect(boolean yesno);
boolean MQTT_connection_status(void);
boolean MQTT_subscribe(const char *topic, uint8_t QoS);
boolean MQTT_unsubscribe(const char *topic);
boolean MQTT_publish(const char *topic, const char *message, uint16_t contentLength, uint8_t QoS, uint8_t retain);
boolean MQTT_dataFormatHex(boolean yesno);
void check_mqtt_msj();
boolean HTTP_init();
boolean HTTP_term();
void HTTP_para_start(char *parameter, boolean quoted);
boolean HTTP_para_end(boolean quoted);
boolean HTTP_para(char *parameter, const char *value);
boolean HTTP_para_number(char *parameter, int32_t value);
boolean HTTP_data(uint32_t size, uint32_t maxTime);
boolean HTTP_action(uint8_t method, uint16_t *status, uint16_t *datalen, int32_t timeout);
boolean HTTP_readall(uint16_t *datalen);
boolean HTTP_ssl(boolean onoff);
boolean HTTP_GET_start(char *url, uint16_t *status, uint16_t *datalen);
void HTTP_GET_end(void);
boolean HTTP_POST_start(char *url, char *contenttype, const uint8_t *postdata, uint16_t postdatalen, uint16_t *status, uint16_t *datalen);
void HTTP_POST_end(void);
void set_user_agent(char *puseragent);
boolean HTTP_setup(char *url);
boolean expect_reply(char *reply, uint32_t timeout);
uint8_t getSMSInterrupt(void);
boolean setSMSInterrupt(uint8_t i);
int8_t getNumSMS(void);
boolean readSMS(uint8_t i, char *smsbuff, uint16_t maxlen, uint16_t *readlen);
boolean getSMSSender(uint8_t i, char *sender, int senderlen);
boolean sendSMS(const char *smsaddr, const char *smsmsg);
boolean deleteSMS(uint8_t i);
boolean deleteAllSMS();

boolean enable_GPRS(boolean onoff);
boolean enableNTP();
boolean setNetworkTime();

#endif