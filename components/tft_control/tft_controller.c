#include "tft_controller.h"

esp_err_t _init_tft_config()
{
    // ========  PREPARE DISPLAY INITIALIZATION  =========
    esp_err_t ret = ESP_OK;
    // === SET GLOBAL VARIABLES ==========================
    // ===================================================
    // ==== Set display type                         =====
    tft_disp_type = DISP_TYPE_ILI9488;
    // ===================================================

    // ===================================================
    // === Set display resolution if NOT using default ===
    // === DEFAULT_TFT_DISPLAY_WIDTH &                 ===
    // === DEFAULT_TFT_DISPLAY_HEIGHT                  ===
    _width = DEFAULT_TFT_DISPLAY_WIDTH;   // smaller dimension
    _height = DEFAULT_TFT_DISPLAY_HEIGHT; // larger dimension
    // ===================================================

    // ===================================================
    // ==== Set maximum spi clock for display read    ====
    //      operations, function 'find_rd_speed()'    ====
    //      can be used after display initialization  ====
    max_rdclock = 80000000;
    // ===================================================

    // ====================================================================
    // === Pins MUST be initialized before SPI interface initialization ===
    // ====================================================================
    TFT_PinsInit();

    // ====  CONFIGURE SPI DEVICES(s)  ====================================================================================

    spi_lobo_device_handle_t spi;

    spi_lobo_bus_config_t buscfg = {
        .miso_io_num = PIN_NUM_MISO_TFT, // set SPI MISO pin
        .mosi_io_num = PIN_NUM_MOSI_TFT, // set SPI MOSI pin
        .sclk_io_num = PIN_NUM_CLK_TFT,  // set SPI CLK pin
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        //.max_transfer_sz = 8*1024,
        .max_transfer_sz = 6 * 1024,
    };
    spi_lobo_device_interface_config_t devcfg = {
        .clock_speed_hz = 80000000,         // Initial clock out at 8 MHz
        .mode = 0,                          // SPI mode 0
        .spics_io_num = -1,                 // we will use external CS pin
        .spics_ext_io_num = PIN_NUM_CS_TFT, // external CS pin
        .flags = LB_SPI_DEVICE_HALFDUPLEX,  // ALWAYS SET  to HALF DUPLEX MODE!! for display spi
    };
    // ========================================================================================================

    ESP_LOGI(TAG_TFT_CONTROLLER, "Pins used: miso=%d, mosi=%d, sck=%d, cs=%d", PIN_NUM_MISO, PIN_NUM_MOSI, PIN_NUM_CLK, PIN_NUM_CS);

    // ==================================================================
    // ==== Initialize the SPI bus and attach the LCD to the SPI bus ====

    ret = spi_lobo_bus_add_device(SPI_BUS, &buscfg, &devcfg, &spi);
    if (ret != ESP_OK)
    {
        ret = -1;
        ESP_LOGE(TAG_TFT_CONTROLLER, "spi_lobo_bus_add_device(SPI_BUS, &buscfg, &devcfg, &spi) FAIL");
        goto fail;
    }
    ESP_LOGI(TAG_TFT_CONTROLLER, "SPI: display device added to spi bus (%d)", SPI_BUS);
    disp_spi = spi;

    // ==== Test select/deselect ====
    ret = spi_lobo_device_select(spi, 1);
    if (ret != ESP_OK)
    {
        ret = -2;
        ESP_LOGE(TAG_TFT_CONTROLLER, "spi_lobo_device_select(spi, 1) FAIL");
        goto fail;
    }
    ret = spi_lobo_device_deselect(spi);
    if (ret != ESP_OK)
    {
        ret = -3;
        ESP_LOGE(TAG_TFT_CONTROLLER, "spi_lobo_device_deselect(spi) FAIL");
        goto fail;
    }
    ESP_LOGI(TAG_TFT_CONTROLLER, "SPI: attached display device, speed=%u\r\n", spi_lobo_get_speed(spi));
    ESP_LOGI(TAG_TFT_CONTROLLER, "SPI: bus uses native pins: %s\r\n", spi_lobo_uses_native_pins(spi) ? "true" : "false");

    // ================================
    // ==== Initialize the Display ====

    ESP_LOGI(TAG_TFT_CONTROLLER, "SPI: display init...\r\n");
    TFT_display_init();
    ESP_LOGI(TAG_TFT_CONTROLLER, "OK\r\n");

    // ---- Detect maximum read speed ----
    max_rdclock = find_rd_speed();
    ESP_LOGI(TAG_TFT_CONTROLLER, "SPI: Max rd speed = %u\r\n", max_rdclock);

    // ==== Set SPI clock used for display operations ====
    spi_lobo_set_speed(spi, 40000000);
    //spi_lobo_set_speed(spi, DEFAULT_SPI_CLOCK);
    ESP_LOGI(TAG_TFT_CONTROLLER, "SPI: Changed speed to %u\r\n", spi_lobo_get_speed(spi));

    font_rotate = 0;
    text_wrap = 1;
    font_transparent = 0;
    font_forceFixed = 0;
    gray_scale = 0;
    TFT_setGammaCurve(DEFAULT_GAMMA_CURVE);
    TFT_setRotation(PORTRAIT);
    TFT_setFont(DEFAULT_FONT, NULL);
fail:
    return ret;
}

void pintar_pantalla(){
    TFT_fillScreen((color_t){10,10,50});
    TFT_drawCircle(160, 240, 30, (color_t){210,210,250});
}


void escribir_temperatura(void *args){
    char temp_text[20];
    _fg = (color_t){200,210,250};
    while(1){
        sprintf(temp_text, "%.2f", *temp_ptr);
        TFT_fillCircle(160, 240, 30, (color_t){20,21,25});
        TFT_print(temp_text, 160, 240);
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
}