#ifndef __TFT_CONTROLLER_H__
#define __TFT_CONTROLLER_H__
#include <stdio.h>
#include <ctype.h>
#include <stdint.h>
#include "tft.h"
#include "cJSON.h"
#include <esp_err.h>
#include <esp_log.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define PIN_NUM_MISO_TFT -1 // SPI MISO
#define PIN_NUM_MOSI_TFT 13 // SPI MOSI
#define PIN_NUM_CLK_TFT 14  // SPI CLOCK pin
#define PIN_NUM_CS_TFT 15   // Display CS pin
#define PIN_NUM_DC_TFT 9    // Display command/data pin

#define PIN_NUM_RST_TFT 25 // GPIO used for RESET control (#16)

#define SPI_BUS TFT_HSPI_HOST

//LOG TAG
static const char *TAG_TFT_CONTROLLER = "TFT_CONTROLLER";


float *temp_ptr;
float *hume_ptr;

esp_err_t _init_tft_config();

void pintar_pantalla();
void escribir_temperatura(void *arg);
#endif
