#ifndef __EXPANDER_CONTROLLER_H__
#define __EXPANDER_CONTROLLER_H__

#include <driver/gpio.h>
#include <mcp23017.h>

#include "esp_types.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "soc/timer_group_struct.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "esp_log.h"

//I2C CONFIG
#define SDA_GPIO                22
#define SCL_GPIO                21
#define I2C_PORT                0

//HARDWARE DEFINITIONS
#define NUM_LEDS                12
#define NUM_COLS                3

#define NUM_EXPA                3
#define EXP_PINS                16

#define DUTY_MAX                16

#define LED_RED                 0
#define LED_GREEN               1
#define LED_BLUE                2


#define NUM_OPTO                2
#define NUM_RELA                2


//TIMER DEFINITIONS
#define TIMER_GROUP_PWM         TIMER_GROUP_0
#define TIMER_IDX_PWM           TIMER_1

#define TIMER_DIVIDER           16                                  //  Hardware timer clock divider
#define TIMER_SCALE             (TIMER_BASE_CLK / TIMER_DIVIDER)    // convert counter value to seconds
#define TIMER_INTERVAL_SEC      (0.00125)                           // Timer 800Hz

//GPIO STRUCTS
typedef struct { 
    uint8_t     add_offset; //The base address is 20H, this value should be 0,1,2;
    uint8_t     GPIO;       //GOES FROM 0 TO 15;
    uint8_t     value;
    uint8_t     cont;
    uint8_t     duty;
    mcp23017_gpio_mode_t mode;
} gpio_exp;

typedef struct {
    gpio_exp    color[3];   //0-R 1-G 2-B
    uint8_t     LED_NUM;    // GOES FROM 0 TO 11;
} RGB_LED;



//HARDWARE GLOBAL DECLARATIONS
RGB_LED _LEDS[NUM_LEDS]; 
i2c_dev_t DEV_EXPANDER;

gpio_exp _OPTOCUPLES[NUM_OPTO];
gpio_exp _RELAYS[NUM_RELA];
gpio_exp _TOUCH_AUX;
gpio_exp _LCD_LEDS;
gpio_exp _MODEM_PWR;
gpio_exp _MODEM_RST;
gpio_exp _NFC_IRQ;

//TIMER EVENT 
typedef struct {
    uint16_t out_word[NUM_EXPA];
} timer_event_t;
xQueueHandle timer_queue;

//LOG TAG
static const char *TAG_EXP_CONTROLLER = "EXPANDER_CONTROLLER";

/**
 * @brief Initialize driver mcp23017
 * Configure port of the 3 expander according to the hardware desing
 * @return `ESP_OK` on success
 */
int _init_gpio_i2c_configuration();

/**
 * @brief Initialize gpio struct
 * Load values into the gpio according to the hardware desing
 * @param gpio      Pointer to gpio struct
 * @param dir_off   offset from I2C address 
 * @param gpio_num  GPIO number on expander
 * @param mode      pin mode operation(Input, Output)
 * @return
 */
void config_gpio(gpio_exp *gpio, uint8_t dir_off, uint8_t gpio_num, mcp23017_gpio_mode_t mode);

/**
 * @brief Read GPIO pin level
 * Load value of gpio, according to the inicial configuration
 * @param gpio      Pointer to GPIO (gpio_exp)
 * @return 
 */
void read_value(gpio_exp *gpio);

/**
 * @brief Read GPIO port level
 * @param exp_num       Expander number
 * @param[out] value    16-bit GPIO port value, 0 bit for PORTA/GPIO0..15 bit for PORTB/GPIO7
 * @return 
 */
void read_value_port(uint8_t exp_num, uint16_t *value);

/**
 * @brief Set GPIO pin level
 * write gpio.value on pin 
 * @param gpio      Pointer to GPIO (gpio_exp)
 * @return 
 */
void write_value(gpio_exp *gpio);

/**
 * @brief Write value to GPIO port
 * 
 * @param exp_num   Expander number
 * @param value     GPIO port value, 0 bit for PORTA/GPIO0..15 bit for PORTB/GPIO7
 * @return 
 */
void write_value_port(uint8_t exp_num, uint16_t value);

/**
 * @brief Initialize timer for PWM on LEDs
 * Initialize timer descriptor, set the counter value and set alarm
 * Start the timer, create a queue for timer interrupt
 * @return 0 if ok
 */
esp_err_t _init_timer_config();

/**
 * @brief Callback function, timer ISR.
 * Reconfigure timer to setup the alarm 
 * Call update_values
 * @return 
 */
void IRAM_ATTR timer_exp_isr(void *para);

/**
 * @brief Call write_values_pwm for every GPIO LED.
 * @return 
 */
void IRAM_ATTR update_pwm_counters();

/**
 * @brief Update value of GPIO acording counter and duty
 * Increment counter of GPIO
 * @param gpio      Pointer to GPIO (gpio_exp)
 * @return 
 */
void IRAM_ATTR write_values_pwm(gpio_exp *gpio);

/**
 * @brief Update the port value output of the expanders
 * Generate a timer_event_t with the information and put it into the timer queue 
 * @return 
 */
void IRAM_ATTR update_outputs_words();

/**
 * @brief Receive timer_event_t from timer_queue and write those values into the ports
 * @return 
 */
void task_update_values(void *arg);

/**
 * @brief Read values from the Optocouples
 * @return 
 */
void read_optocuples();

/*
---------------------------- CONTENIDO AGREGADO ------------------------------------------
------------------- NO UTILIZA EL TIMER PARA LAS LUCES, SOLO 1 y 0 -----------------------
*/

void task_update_and_write_values(void *arg);
void update_values_from_config();
void get_port_value_num(uint16_t *values);


#endif