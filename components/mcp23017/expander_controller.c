#include "expander_controller.h"
#include <mcp23017.h>

int _init_gpio_i2c_configuration()
{

    //DRIVER INIT
    if (i2cdev_init() != ESP_OK)
    {
        ESP_LOGE(TAG_EXP_CONTROLLER, "Could not init I2Cdev library");
        return -1;
    }
    if (mcp23017_init_desc(&DEV_EXPANDER, I2C_PORT, MCP23017_I2C_ADDR_BASE, SDA_GPIO, SCL_GPIO) != ESP_OK)
    {
        ESP_LOGE(TAG_EXP_CONTROLLER, "Could not init device descriptor");
        return -2;
    }
    //CONFIGURE LEDS WITH VALUE 0 AND OUTPUT MODE
    uint8_t id_leds, id_cols, gpio = 0, dir_off = 0, cont = 0;
    for (id_leds = 0; id_leds < NUM_LEDS; id_leds++)
    {
        _LEDS[id_leds].LED_NUM = id_leds;
        for (id_cols = 0; id_cols < NUM_COLS; id_cols++)
        {
            if (cont > 0 && cont % (EXP_PINS - 1) == 0)
                dir_off++;
            config_gpio(&(_LEDS[id_leds].color[id_cols]), dir_off, gpio, MCP23017_GPIO_OUTPUT);
            gpio = (gpio + 1) % (EXP_PINS - 1);
            cont++;
        }
    }

    //CONFIGURE _NFC_IRQ WITH VALUE 0 AND OUTPUT MODE
    gpio = 15;
    dir_off = 0;
    config_gpio(&_NFC_IRQ, dir_off, gpio, MCP23017_GPIO_OUTPUT);

    //CONFIGURE _LCD_LEDS WITH VALUE 0 AND OUTPUT MODE
    gpio = 15;
    dir_off = 1;
    config_gpio(&_LCD_LEDS, dir_off, gpio, MCP23017_GPIO_OUTPUT);
    //CONFIGURE _MODEM
    gpio = 6;
    dir_off = 2;
    config_gpio(&_MODEM_PWR, dir_off, gpio++, MCP23017_GPIO_OUTPUT);
    config_gpio(&_MODEM_RST, dir_off, gpio++, MCP23017_GPIO_OUTPUT);

    //CONFIGURE _OPTOCUPLES AS INPUT MODE
    for (id_leds = 0; id_leds < NUM_OPTO; id_leds++)
    {
        config_gpio(_OPTOCUPLES + id_leds, dir_off, gpio, MCP23017_GPIO_INPUT);
        gpio = (gpio + 1) % EXP_PINS;
    }

    gpio = 13 % EXP_PINS;
    //CONFIGURE _TOUCH_AUX WITH VALUE 0 AND OUTPUT MODE
    config_gpio(&_TOUCH_AUX, dir_off, gpio, MCP23017_GPIO_OUTPUT);
    gpio = (gpio + 1) % EXP_PINS;
    //CONFIGURE _RELAYS WITH VALUE 0 AND OUTPUT MODE
    for (id_leds = 0; id_leds < NUM_RELA; id_leds++)
    {
        config_gpio(_RELAYS + id_leds, dir_off, gpio, MCP23017_GPIO_OUTPUT);
        gpio = (gpio + 1) % EXP_PINS;
        //KEEP RELAYS LOW
        _RELAYS[id_leds].value = 0;
        write_value(_RELAYS + id_leds);
    }

    return ESP_OK;
}

void config_gpio(gpio_exp *gpio, uint8_t dir_off, uint8_t gpio_num, mcp23017_gpio_mode_t mode)
{
    gpio->add_offset = dir_off;
    gpio->GPIO = gpio_num;
    gpio->value = 1;
    gpio->duty = 0;
    gpio->cont = 0;
    gpio->mode = mode;
    DEV_EXPANDER.addr = MCP23017_I2C_ADDR_BASE + dir_off;
    mcp23017_set_mode(&DEV_EXPANDER, gpio->GPIO, mode);
    if (mode == MCP23017_GPIO_OUTPUT)
        write_value(gpio);
    ESP_LOGI(TAG_EXP_CONTROLLER, "ADDR %d GPIO %d ", dir_off, gpio_num);
}

void read_value(gpio_exp *gpio)
{
    if (gpio->mode == MCP23017_GPIO_INPUT)
    {
        DEV_EXPANDER.addr = MCP23017_I2C_ADDR_BASE + gpio->add_offset;
        mcp23017_get_level(&DEV_EXPANDER, gpio->GPIO, &(gpio->value));
    }
    else
    {
        ESP_LOGW(TAG_EXP_CONTROLLER, "Warning! tring to read from GPIO %d FROM EXP %d and mode is OUTPUT!!\n", gpio->GPIO, gpio->add_offset);
    }
}

void read_value_port(uint8_t exp_num, uint16_t *value)
{
    i2c_dev_t *d = &(DEV_EXPANDER);
    d->addr = MCP23017_I2C_ADDR_BASE + exp_num;
    mcp23017_port_read(d, value);
}

void write_value(gpio_exp *gpio)
{
    if (gpio->mode == MCP23017_GPIO_OUTPUT)
    {
        DEV_EXPANDER.addr = MCP23017_I2C_ADDR_BASE + gpio->add_offset;
        mcp23017_set_level(&DEV_EXPANDER, gpio->GPIO, gpio->value);
        //ESP_LOGI(TAG_EXP_CONTROLLER, "add_offset %d, GPIO %d, value %d \n", gpio->add_offset, gpio->GPIO, gpio->value);
    }
    else
    {
        ESP_LOGW(TAG_EXP_CONTROLLER, "Warning! tring to write into GPIO %d FROM EXP %d and mode is INPUT!!\n", gpio->GPIO, gpio->add_offset);
    }
}

void write_value_port(uint8_t exp_num, uint16_t value)
{
    i2c_dev_t *d = &(DEV_EXPANDER);
    d->addr = MCP23017_I2C_ADDR_BASE + exp_num;
    mcp23017_port_write(d, value);
}

esp_err_t _init_timer_config()
{
    timer_config_t config;
    config.divider = TIMER_DIVIDER;
    config.counter_dir = TIMER_COUNT_UP;
    config.counter_en = TIMER_PAUSE;
    config.alarm_en = TIMER_ALARM_EN;
    config.intr_type = TIMER_INTR_LEVEL;
    config.auto_reload = TIMER_AUTORELOAD_EN;
    timer_init(TIMER_GROUP_PWM, TIMER_IDX_PWM, &config);

    /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(TIMER_GROUP_PWM, TIMER_IDX_PWM, 0x00000000ULL);

    /* Configure the alarm value and the interrupt on alarm. */
    timer_set_alarm_value(TIMER_GROUP_PWM, TIMER_IDX_PWM, TIMER_INTERVAL_SEC * TIMER_SCALE);
    timer_enable_intr(TIMER_GROUP_PWM, TIMER_IDX_PWM);
    timer_isr_register(TIMER_GROUP_PWM, TIMER_IDX_PWM, timer_exp_isr, NULL, ESP_INTR_FLAG_IRAM, NULL);

    timer_start(TIMER_GROUP_PWM, TIMER_IDX_PWM);

    timer_queue = xQueueCreate(5, sizeof(timer_event_t));
    xTaskCreate(task_update_values, "task_update_values", 2048, NULL, 10, NULL);
    return 0;
}

void IRAM_ATTR timer_exp_isr(void *para)
{
    /* Retrieve the interrupt status
       from the timer that reported the interrupt */
    uint32_t intr_status = TIMERG0.int_st_timers.val;
    if (intr_status)
    {
        update_pwm_counters();
        update_outputs_words();
        TIMERG0.hw_timer[TIMER_IDX_PWM].update.val = 1;
        TIMERG0.int_clr_timers.t1_int_clr = 1;
        /* After the alarm has been triggered
          we need enable it again, so it is triggered the next time */
        TIMERG0.hw_timer[TIMER_IDX_PWM].config.tx_alarm_en = TIMER_ALARM_EN;
    }
}

void IRAM_ATTR update_pwm_counters()
{
    uint8_t id_leds, id_cols;
    for (id_leds = 0; id_leds < NUM_LEDS; id_leds++)
    {
        for (id_cols = 0; id_cols < NUM_COLS; id_cols++)
        {
            gpio_exp *gpio = &(_LEDS[id_leds].color[id_cols]);
            write_values_pwm(gpio);
        }
    }
    write_values_pwm(&_LCD_LEDS);
}

void IRAM_ATTR write_values_pwm(gpio_exp *gpio)
{
    if (gpio->cont < gpio->duty)
        gpio->value = 0;
    else
        gpio->value = 1;
    gpio->cont = (gpio->cont + 1) % DUTY_MAX;
}

void IRAM_ATTR update_outputs_words()
{
    uint8_t id_leds, id_cols;
    timer_event_t evt;
    for (id_leds = 0; id_leds < NUM_EXPA; id_leds++)
    {
        evt.out_word[id_leds] = 0;
    }
    gpio_exp gpio;
    for (id_leds = 0; id_leds < NUM_LEDS; id_leds++)
    {
        for (id_cols = 0; id_cols < NUM_COLS; id_cols++)
        {
            gpio = _LEDS[id_leds].color[id_cols];
            if (gpio.value)
                evt.out_word[gpio.add_offset] ^= 1 << (gpio.GPIO);
        }
    }
    //LCD CONF
    if (_LCD_LEDS.value)
        evt.out_word[_LCD_LEDS.add_offset] ^= 1 << (_LCD_LEDS.GPIO);
    //RELAYS CONF
    for (id_leds = 0; id_leds < NUM_RELA; id_leds++)
    {
        gpio = _RELAYS[id_leds];
        if (gpio.value)
            evt.out_word[gpio.add_offset] ^= 1 << (gpio.GPIO);
    }
    xQueueSendFromISR(timer_queue, &evt, NULL);
}
/*
void print_bits(uint16_t val)
{
    int z = 32768, oct = val;
    char text[20] = {0};
    while (z > 0)
    {
        if (z & oct)
            sprintf(text, "%s1", text);
        else
            sprintf(text, "%s0", text);
        z >>= 1;
    }
    ESP_LOGI(TAG_EXP_CONTROLLER, "%s\t", text);
}

void print_leds()
{
    uint8_t id_leds, id_cols;
    for (id_leds = 0; id_leds < NUM_LEDS; id_leds++)
    {
        for (id_cols = 0; id_cols < NUM_COLS; id_cols++)
        {
            gpio_exp g = _LEDS[id_leds].color[id_cols];
            ESP_LOGI(TAG_EXP_CONTROLLER, "%d-GPIO %d \t value %d \t cont %d \t duty %d\n", _LEDS[id_leds].LED_NUM,
                     g.GPIO, g.value, g.cont, g.duty);
        }
    }
}
*/
void task_update_values(void *arg)
{
    uint8_t id_e;
    while (1)
    {
        timer_event_t evt;
        xQueueReceive(timer_queue, &evt, portMAX_DELAY);
        for (id_e = 0; id_e < NUM_EXPA; id_e++)
        {
            write_value_port(id_e, evt.out_word[id_e]);
            //print_bits(evt.out_word[id_e]);
        }
        //ESP_LOGI(TAG_EXP_CONTROLLER, "\n");
        read_optocuples();
    }
}

void read_optocuples()
{
    uint16_t value;
    uint8_t id_opto;
    read_value_port(2, &value);
    for (id_opto = 0; id_opto < NUM_OPTO; id_opto++)
    {
        gpio_exp *gpio = &(_OPTOCUPLES[id_opto]);
        gpio->value = (value & 1 << gpio->GPIO) > 0 ? 1 : 0;
    }
}

void task_update_and_write_values(void *arg)
{
    uint8_t id_e;
    uint16_t values[NUM_EXPA];
    for (id_e = 0; id_e < NUM_RELA; id_e++)
    {
        _RELAYS[id_e].value = 1;
    }
    while (1)
    {
        update_values_from_config();
        get_port_value_num(values + 0);
        for (id_e = 0; id_e < NUM_EXPA; id_e++)
        {
            //print_bits(~values[id_e]);
            write_value_port(id_e, ~values[id_e]);
        }
        //ESP_LOGI(TAG_EXP_CONTROLLER, "\n");
        read_optocuples();
        vTaskDelay(20 / portTICK_RATE_MS);
    }
}

void update_values_from_config()
{
    uint8_t id_leds, id_cols;
    gpio_exp *gpio;
    for (id_leds = 0; id_leds < NUM_LEDS; id_leds++)
    {
        for (id_cols = 0; id_cols < NUM_COLS; id_cols++)
        {
            gpio = &(_LEDS[id_leds].color[id_cols]);
            if (gpio->duty > 0)
                gpio->value = 1;
            else
                gpio->value = 0;
        }
    }
    gpio = &_LCD_LEDS;
    if (gpio->duty > 0)
        gpio->value = 1;
    else
        gpio->value = 0;
}

void get_port_value_num(uint16_t *values)
{
    uint8_t id_leds, id_cols;

    for (id_leds = 0; id_leds < NUM_EXPA; id_leds++)
    {
        values[id_leds] = 0;
    }
    gpio_exp gpio;
    for (id_leds = 0; id_leds < NUM_LEDS; id_leds++)
    {
        for (id_cols = 0; id_cols < NUM_COLS; id_cols++)
        {
            gpio = _LEDS[id_leds].color[id_cols];
            if (gpio.value)
                values[gpio.add_offset] ^= 1 << (gpio.GPIO);
        }
    }
    //LCD CONF
    if (_LCD_LEDS.value)
        values[_LCD_LEDS.add_offset] ^= 1 << (_LCD_LEDS.GPIO);
    //RELAYS CONF
    for (id_leds = 0; id_leds < NUM_RELA; id_leds++)
    {
        gpio = _RELAYS[id_leds];
        if (gpio.value)
            values[gpio.add_offset] ^= 1 << (gpio.GPIO);
    }
}
