
#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "mesh_control.h"
#include "driver/gpio.h"
#include "expander_controller.h"
#include "DHT22.h"

#ifdef ROOT
#include "modem_control.h"
#else
#include "tft_controller.h"
#endif

#ifdef ROOT
#define LED 26
#else
#define LED 2    
#endif
#define GPIO_OUTPUT_PIN_SEL  ((1ULL<<LED) )

void blink(int num, int wait){
    int i = 0;
    for(;i<num;i++){
        gpio_set_level(LED, 1);
        vTaskDelay(wait / portTICK_PERIOD_MS);
        gpio_set_level(LED, 0);
        vTaskDelay(wait / portTICK_PERIOD_MS);
    }
}

void app_main(void)
{
    
   #ifdef SCREEN 
    _init_gpio_i2c_configuration();
    temp_ptr = &temp_mesh;
    hume_ptr = &hume_mesh;
    #endif
    
#ifdef ROOT
    strcpy(APN, "internet.movistar.cr");
    strcpy(USER, "movistarcr");
    strcpy(PWD, "movistarcr");
    delay_start_up();
#else
    _init_tft_config();
    pintar_pantalla();    
#endif

    mesh_layer = -1;
    iniciar_mesh_red();
    gpio_config_t io_conf = {};
    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);
    #ifdef SCREEN 
    xTaskCreate(escribir_temperatura, "Ecrb_temp", 3072, NULL, 5, NULL);
    #endif
    hume_mesh = 0.0;
    temp_mesh = 0.0; 
    while (1)
    {
        //se captura la informacion de sensor DHT22 en los punteros hume_mesh y temp_mesh
        hume_mesh = getHumidity(); 
        temp_mesh = getTemperature();
        printf("=== Reading DHT ===\n" );
		int ret = readDHT();
		
		errorHandler(ret);

        //se imprime en pantalla los datos de temperatura y humedad
        printf( "Hum %.1f\n", hume_mesh);
		printf( "Tmp %.1f\n", temp_mesh);
		
		vTaskDelay( 3000 / portTICK_RATE_MS );

        ESP_LOGI("MAIN", "mesh_layer %d", mesh_layer);
        blink(mesh_layer, 300);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    
}
